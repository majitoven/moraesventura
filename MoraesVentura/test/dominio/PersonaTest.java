package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mariaventura
 */
public class PersonaTest {

    private Persona unaPersona;
    private ArrayList<Foto> miListaFotos;
    private ArrayList<Documento> miListaDocumentos;
    private ArrayList<Historia> miListaHistorias;
    private Foto miFoto;
    private Documento unDocumento;
    private Persona padre1;
    private Persona padre2;
    private Historia unaHistoria;

    @Before
    public void setUp() {
        unaHistoria = new Historia();
        padre1= new Persona();
        padre2= new Persona();
        miFoto = new Foto();
        unaPersona = new Persona();
        miListaFotos = new ArrayList<>();
        miListaDocumentos = new ArrayList<>();
        miListaHistorias = new ArrayList<>();
        unDocumento = new Documento();

    }

    @Test
    public void testGetNombre() {
        String expResult = "Camila";
        unaPersona.setNombre(expResult);
        String result = unaPersona.getNombre();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetApellido() {
        String expResult = "Gomez";
        unaPersona.setApellido(expResult);
        String result = unaPersona.getApellido();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetLugarNacimiento() {
        String expResult = "Montevideo";
        unaPersona.setLugarNacimiento(expResult);
        String result = unaPersona.getLugarNacimiento();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetFechaNacimiento() {
        String expResult = "10/10/1967";
        unaPersona.setFechaNacimiento(expResult);
        String result = unaPersona.getFechaNacimiento();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetNacionalidad() {
        String expResult = "Uruguay";
        unaPersona.setNacionalidad(expResult);
        String result = unaPersona.getNacionalidad();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDireccionFoto() {
        String expResult = "/src/recursos/fotoPersona.png";
        unaPersona.setDireccionFoto(expResult);
        String result = unaPersona.getDireccionFoto();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetListaFotos() {
        ArrayList<Foto> expResult = miListaFotos;
        unaPersona.setListaFotos(expResult);
        ArrayList<Foto> result = unaPersona.getListaFotos();
        assertEquals(expResult, result);
    }

    @Test
    public void testAgregarFoto() {

        Foto unaFoto = miFoto;
        unaPersona.agregarFoto(unaFoto);
    }

    @Test
    public void testEliminarFoto() {
        Foto unaFoto = miFoto;
        unaPersona.eliminarFoto(unaFoto);
    }

    @Test
    public void testLargoListaFotos() {
        int expResult = 0;
        unaPersona.setListaFotos(miListaFotos);
        int result = unaPersona.largoListaFotos();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetListaDocumentos() {

        ArrayList<Documento> expResult = miListaDocumentos;
        unaPersona.setListaDocumentos(expResult);
        ArrayList<Documento> result = unaPersona.getListaDocumentos();
        assertEquals(expResult, result);
    }

    @Test
    public void testAgregarDocumento() {

        unaPersona.agregarDocumento(unDocumento);
    }

    @Test
    public void testEliminarDocumento() {
        unaPersona.eliminarDocumento(unDocumento);
    }

    @Test
    public void testLargoListaDocumentos() {
        int expResult = 0;
        unaPersona.setListaDocumentos(miListaDocumentos);
        int result = unaPersona.largoListaDocumentos();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetSexo() {
        int expResult = 2;
        unaPersona.setSexo(2);
        int result = unaPersona.getSexo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetListaHistorias() {
        ArrayList<Historia> expResult = miListaHistorias;
        unaPersona.setListaHistorias(miListaHistorias);
        ArrayList<Historia> result = unaPersona.getListaHistorias();
        assertEquals(expResult, result);
    }

    @Test
    public void testAgregarHistoria() {
        unaPersona.agregarHistoria(unaHistoria);
    }

    @Test
    public void testEliminarHistoria() {
        unaPersona.eliminarHistoria(unaHistoria);
    }

    @Test
    public void testLargoListaHistorias() {
        int expResult = 0;
        unaPersona.setListaHistorias(miListaHistorias);
        int result = unaPersona.largoListaHistorias();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPadre1() {
        Persona expResult = padre1;
        unaPersona.setPadre1(expResult);
        Persona result = unaPersona.getPadre1();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPadre2() {
        Persona expResult = padre2;
        unaPersona.setPadre2(expResult);
        Persona result = unaPersona.getPadre2();
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {

        Object o = unaPersona;
        boolean expResult = true;
        boolean result = unaPersona.equals(o);
        assertEquals(expResult, result);
    }

}
