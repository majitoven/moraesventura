package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mariaventura
 */
public class HistoriaTest {
    
private Historia miHistoria;

    @Before
    public void setUp() {
        miHistoria = new Historia();
    }

    @Test
    public void testGetTitulo() {
        String expResult = "Verano 2004 en Punta del Este";
        miHistoria.setTitulo(expResult);
        String result = miHistoria.getTitulo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetContenido() {
        String expResult = "Fuimos a la playa en el verano del 2004 con.....";
        miHistoria.setContenido(expResult);
        String result = miHistoria.getContenido();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetFecha() {
        String expResult = "10/10/2006";
        miHistoria.setFecha(expResult);
        String result = miHistoria.getFecha();
        assertEquals(expResult, result);
    }


}
