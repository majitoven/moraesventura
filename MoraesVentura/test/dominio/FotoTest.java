package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class FotoTest {
    
  private Foto miFoto;

    @Before
    public void setUp() {
        miFoto = new Foto();
    }

    @Test
    public void testGetTitulo() {
        String expResult = "Foto para probar el obligatorio";
        miFoto.setTitulo(expResult);
        String result = miFoto.getTitulo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDireccion() {
        String expResult = "/src/resources/fotoObligatorio.png";
        miFoto.setDireccion(expResult);
        String result = miFoto.getDireccion();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetFecha() {
        String expResult = "21/2/1964";
        miFoto.setFecha(expResult);
        String result = miFoto.getFecha();
        assertEquals(expResult, result);
    }

}
