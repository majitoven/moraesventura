package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class SistemaTest {

    private Sistema miSistema;
    private ArrayList<Persona> miListaPersonas;
    private ArrayList<Union> miListaUniones;
    private ArrayList<Persona> miListaHijos;
    private ArrayList<Persona> miListaRetorno;
    private Persona p1;
    private Persona p2;
    private Persona p3;
    private Persona p4;
    private Persona hijo1;
    private Persona hijo2;
    private Persona hijo3;
    private Union u;
    private Union u2;

    @Before
    public void setUp() {
        u2 = new Union();
        u = new Union();
        p1 = new Persona();
        p2 = new Persona();
        p3 = new Persona();
        p4 = new Persona();
        p1.setNombre("Maria");
        p1.setApellido("Ventura");
        p2.setNombre("Victoria");
        p2.setApellido("Moraes");
        p3.setNombre("Martina");
        p3.setNombre("Morales");
        p4.setNombre("Victor");
        p4.setNombre("Garcia");
        hijo1 = new Persona();
        hijo2 = new Persona();
        hijo3 = new Persona();
        hijo1.setNombre("Mateo");
        hijo1.setApellido("Rodriguez");
        hijo2.setNombre("Juan");
        hijo2.setApellido("Rodriguez");
        hijo3.setNombre("Mariano");
        hijo3.setApellido("Gomez");
        miSistema = new Sistema();
        miListaPersonas = new ArrayList<>();
        miListaUniones = new ArrayList<>();
        miListaHijos = new ArrayList<>();
        miListaRetorno = new ArrayList<>();
    }

    @Test
    public void testGetListaPersonas() {
        ArrayList<Persona> expResult = miListaPersonas;
        miSistema.setListaPersonas(miListaPersonas);
        ArrayList<Persona> result = miSistema.getListaPersonas();
        assertEquals(expResult, result);
    }

    @Test
    public void testAgregarPersona() {
        Persona expResult = p1;
        miSistema.agregarPersona(p1);
        Persona result = miSistema.getListaPersonas().get(0);
        assertEquals(expResult, result);
    }

    @Test
    public void testEliminarPersona() {
        int expResult = 0;
        Persona per1 = p1;
        miSistema.agregarPersona(per1);
        miSistema.eliminarPersona(per1);
        assertEquals(expResult, miSistema.largoListaPersonas());

    }

    @Test
    public void testGetListaUniones() {
        ArrayList<Union> expResult = miListaUniones;
        miSistema.setListaUniones(miListaUniones);
        ArrayList<Union> result = miSistema.getListaUniones();
        assertEquals(expResult, result);
    }

    @Test
    public void testAgregarUnion() {
        int expResult = 1;
        u.setPersona1(p1);
        u.setPersona2(p2);
        miSistema.agregarUnion(u);
        assertEquals(expResult, miSistema.largoListaUniones());
    }

    @Test
    public void testEliminarUnion() {
        int expResult = 0;
        u.setPersona1(p1);
        u.setPersona2(p2);
        miSistema.agregarUnion(u);
        miSistema.eliminarUnion(u);
        assertEquals(expResult, miSistema.largoListaUniones());
    }

    @Test
    public void testListaPersonasVacia() {
        boolean expResult = true;
        boolean result = miSistema.largoListaPersonas() == 0;
        assertEquals(expResult, result);
    }

    @Test
    public void testLargoListaPersonas() {
        int expResult = 0;
        int result = miSistema.getListaPersonas().size();
        assertEquals(expResult, result);
    }

    @Test
    public void testLargoListaUniones() {
        int expResult = 0;
        int result = miSistema.getListaUniones().size();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetListaPersonasSolteras() {
        miListaPersonas.add(p1);
        miListaPersonas.add(p2);
        ArrayList<Persona> expResult = miListaPersonas;
        miSistema.setListaPersonasSolteras(miListaPersonas);
        ArrayList<Persona> result = miSistema.getListaPersonasSolteras();
        assertEquals(expResult, result);
    }

    @Test
    public void testAgregarPersonaSoltera() {
        int expResult = 1;
        miSistema.agregarPersonaSoltera(p1);
        assertEquals(expResult, miSistema.largoListaPersonasSolteras());
    }

    @Test
    public void testEliminarPersonaSoltera() {
        int expResult = 0;
        miSistema.agregarPersonaSoltera(p1);
        miSistema.eliminarPersonaSoltera(p1);
        assertEquals(expResult, miSistema.largoListaPersonasSolteras());
    }

    @Test
    public void testLargoListaPersonasSolteras() {
        int expResult = 0;
        int result = miSistema.getListaPersonasSolteras().size();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetListaPersonasSinPadres() {
        miListaPersonas.add(p1);
        miListaPersonas.add(p2);
        ArrayList<Persona> expResult = miListaPersonas;
        miSistema.setListaPersonasSinPadres(miListaPersonas);
        ArrayList<Persona> result = miSistema.getListaPersonasSinPadres();
        assertEquals(expResult, result);
    }

    @Test
    public void testAgregarPersonaSinPadres() {
        int expResult = 1;
        miSistema.agregarPersonaSinPadres(p1);
        assertEquals(expResult, miSistema.largoListaPersonasSinPadres());
    }

    @Test
    public void testEliminarPersonaSinPadres() {
        int expResult = 0;
        miSistema.agregarPersonaSinPadres(p1);
        miSistema.eliminarPersonaSinPadres(p1);
        assertEquals(expResult, miSistema.largoListaPersonasSinPadres());
    }

    @Test
    public void testLargoListaPersonasSinPadres() {
        int expResult = 0;
        int result = miSistema.getListaPersonasSinPadres().size();
        assertEquals(expResult, result);
    }

    @Test
    public void testHayRecuerdos() {
        boolean expResult = false;
        boolean result = miSistema.hayRecuerdos();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPareja() {
        u.setPersona1(p1);
        u.setPersona2(p2);
        miSistema.agregarUnion(u);
        Union expResult = u;
        Union result = miSistema.getPareja(p1);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetMiembroPareja() {
        u.setPersona1(p1);
        u.setPersona2(p2);
        miSistema.agregarUnion(u);
        Persona expResult = p2;
        Persona result = miSistema.getMiembroPareja(p1);
        assertEquals(expResult, result);
    }

    @Test
    public void testListaValidadaPersonasSinPadres() {
        u.setPersona1(p1);
        u.setPersona2(p2);
        miSistema.agregarUnion(u);
        u2.setPersona1(p3);
        u2.setPersona2(p4);
        miSistema.agregarUnion(u2);
        u2.agregarHijo(hijo3);
        u.agregarHijo(p3);
        ArrayList<Persona> expResult = miListaRetorno;
        ArrayList<Persona> result = miSistema.listaValidadaPersonasSinPadres(u2);
        assertEquals(expResult, result);
    }

    @Test
    public void testPersonasConRecuerdos() {
        ArrayList<Persona> expResult = miListaPersonas;
        ArrayList<Persona> result = miSistema.personasConRecuerdos();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetRaicesCandidatas() {
        u.setPersona1(p1);
        u.setPersona2(p2);
        miListaUniones.add(u);
        miSistema.agregarPersonaSinPadres(p1);
        miSistema.agregarPersonaSinPadres(p2);
        miSistema.agregarUnion(u);
        ArrayList<Union> expResult = miListaUniones;
        ArrayList<Union> result = miSistema.getRaicesCandidatas();
        assertEquals(expResult, result);
    }

    @Test
    public void testSonHermanos() {
        u.setPersona1(p1);
        u.setPersona2(p2);
        miListaHijos.add(hijo1);
        miListaHijos.add(hijo2);
        miSistema.agregarUnion(u);
        u.setListaHijos(miListaHijos);
        boolean expResult = true;
        boolean result = miSistema.sonHermanos(hijo1, hijo2);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetListaPersonasSolterasConPosiblesHijos() {
        ArrayList<Union> expResult = miListaUniones;
        ArrayList<Union> result = miSistema.getListaPersonasSolterasConPosiblesHijos();
        assertEquals(expResult, result);
    }

    @Test
    public void testEsParejaDe() {
        u.setPersona1(p1);
        u.setPersona2(p2);
        miSistema.agregarUnion(u);
        Persona expResult = p1;
        Persona result = miSistema.esParejaDe(p2);
        assertEquals(expResult, result);
    }

}
