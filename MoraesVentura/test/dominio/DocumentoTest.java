package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class DocumentoTest {

    private Documento miDoc;

    @Before
    public void setUp() {
        miDoc = new Documento();
    }

    @Test
    public void testGetTitulo() {
        String expResult = "Documentacion de ingenieria de software";
        miDoc.setTitulo(expResult);
        String result = miDoc.getTitulo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDireccion() {
        String expResult = "/src/resources/ingSoft.pdf";
        miDoc.setDireccion(expResult);
        String result = miDoc.getDireccion();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetFecha() {
        String expResult = "10/10/2006";
        miDoc.setFecha(expResult);
        String result = miDoc.getFecha();
        assertEquals(expResult, result);
    }


}
