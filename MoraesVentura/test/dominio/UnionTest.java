package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class UnionTest {

    private Persona unaPersona1;
    private Persona unaPersona2;
    private Persona hijo;
    private Union miUnion;
    ArrayList<Persona> listaH;

    @Before
    public void setUp() {
        miUnion = new Union();
        unaPersona1 = new Persona();
        unaPersona2 = new Persona();
        hijo = new Persona();
        listaH = new ArrayList<>();
    }

    @Test
    public void testGetPersona1() {
        Persona expResult = unaPersona1;
        miUnion.setPersona1(expResult);
        Persona result = miUnion.getPersona1();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPersona2() {
        Persona expResult = unaPersona2;
        miUnion.setPersona2(expResult);
        Persona result = miUnion.getPersona2();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTipo() {
        int expResult = 0;
        miUnion.setTipo(expResult);
        int result = miUnion.getTipo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetFechaInicio() {
        String expResult = "20/12/1996";
        miUnion.setFechaInicio(expResult);
        String result = miUnion.getFechaInicio();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetFechaFin() {
        String expResult = "10/12/2002";
        miUnion.setFechaFin(expResult);
        String result = miUnion.getFechaFin();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetListaHijos() {
        ArrayList<Persona> expResult = listaH;
        miUnion.setListaHijos(expResult);
        ArrayList<Persona> result = miUnion.getListaHijos();
        assertEquals(expResult, result);
    }

    @Test
    public void testAgregarHijo() {
        miUnion.agregarHijo(hijo);
    }

    @Test
    public void testEliminarHijo() {
        miUnion.eliminarHijo(hijo);
    }

    @Test
    public void testLargoListaHijos() {
        int expResult = 0;
        int result = miUnion.largoListaHijos();
        assertEquals(expResult, result);
    }

}