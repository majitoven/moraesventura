package obligatorioingsoft;

import dominio.*;
import interfaz.*;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class ObligatorioIngSoft {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Sistema sis;
        try {
            FileInputStream archivo = new FileInputStream("datos");
            BufferedInputStream buffer = new BufferedInputStream(archivo);
            ObjectInputStream obj = new ObjectInputStream(buffer);
            sis = (Sistema) obj.readObject();
        } catch (FileNotFoundException e) {
            sis = new Sistema();
        }
        Ventana v = new Ventana(sis);
        v.setVisible(true);
        v.setResizable(false);
    }

}

