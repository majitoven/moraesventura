package dominio;

import java.io.Serializable;
import java.util.ArrayList;

public class Union implements Serializable {

    private Persona persona1;
    private Persona persona2;
    private int tipo;
    //Siendo tipo 0.Matrimonio, 1.Concubinato
    private String fechaInicio;
    private String fechaFin;
    //Las fechas tienen el siguiente formato: DD/MM/AA
    private ArrayList<Persona> listaHijos;

    public Union(Persona p1, Persona p2, int t, String fechaI, String fechaF, ArrayList<Persona> listaHijos) {
        this.setPersona1(p1);
        this.setPersona2(p2);
        this.setTipo(t);
        this.setFechaInicio(fechaI);
        this.setFechaFin(fechaF);
        this.setListaHijos(listaHijos);
    }

    public Union() {
        this.setPersona1(null);
        this.setPersona2(null);
        this.setTipo(-1);
        this.setFechaInicio("Sin fecha");
        this.setFechaFin("Sin fecha");
        listaHijos = new ArrayList<>();
    }

    public Persona getPersona1() {
        return persona1;
    }

    public void setPersona1(Persona unaPrimerPersona) {
        persona1 = unaPrimerPersona;
    }

    public Persona getPersona2() {
        return persona2;
    }

    public void setPersona2(Persona unaSegundaPersona) {
        persona2 = unaSegundaPersona;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int unTipo) {
        tipo = unTipo;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String unaFechaInicio) {
        fechaInicio = unaFechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String unaFechaFin) {
        fechaFin = unaFechaFin;
    }

    public ArrayList<Persona> getListaHijos() {
        return listaHijos;
    }

    public void setListaHijos(ArrayList<Persona> unaListaHijos) {
        listaHijos = unaListaHijos;
    }

    public void agregarHijo(Persona unaPersona) {
        this.getListaHijos().add(unaPersona);
    }

    public void eliminarHijo(Persona unaPersona) {
        listaHijos.remove(unaPersona);
    }


    public int largoListaHijos() {
        return listaHijos.size();
    }

    @Override
    public String toString() {
        return this.getPersona1().getNombre()+ " "+this.getPersona1().getApellido() + " y " + this.getPersona2().getNombre()+" "+ this.getPersona2().getApellido();
    }

}
