package dominio;

import java.io.*;
import java.util.*;

public class Sistema implements Serializable {

    private ArrayList<Persona> listaPersonas;
    private ArrayList<Union> listaUniones;
    private ArrayList<Persona> listaPersonasSolteras;
    private ArrayList<Persona> listaPersonasSinPadres;

    public Sistema() {
        listaPersonas = new ArrayList<>();
        listaUniones = new ArrayList<>();
        listaPersonasSolteras = new ArrayList<>();
        listaPersonasSinPadres = new ArrayList<>();
    }

    public ArrayList<Persona> getListaPersonas() {
        return listaPersonas;
    }

    public void setListaPersonas(ArrayList<Persona> unaListaPersonas) {
        listaPersonas = unaListaPersonas;
    }

    public void agregarPersona(Persona unaPersona) {
        this.getListaPersonas().add(unaPersona);
    }

    public void eliminarPersona(Persona unaPersona) {
        listaPersonas.remove(unaPersona);
    }

    public ArrayList<Union> getListaUniones() {
        return listaUniones;
    }

    public void setListaUniones(ArrayList<Union> unaListaUniones) {
        listaUniones = unaListaUniones;
    }

    public void agregarUnion(Union unaUnion) {
        this.getListaUniones().add(unaUnion);
    }

    public void eliminarUnion(Union unaUnion) {
        listaUniones.remove(unaUnion);
    }

    public boolean listaPersonasVacia() {
        return listaPersonas.isEmpty();
    }

    public int largoListaPersonas() {
        return listaPersonas.size();
    }

    public int largoListaUniones() {
        return listaUniones.size();
    }

    public ArrayList<Persona> getListaPersonasSolteras() {
        return listaPersonasSolteras;
    }

    public void setListaPersonasSolteras(ArrayList<Persona> unaListaPersonasSolteras) {
        listaPersonasSolteras = unaListaPersonasSolteras;
    }

    public void agregarPersonaSoltera(Persona unaPersonaSoltera) {
        this.getListaPersonasSolteras().add(unaPersonaSoltera);
    }

    public void eliminarPersonaSoltera(Persona unaPersonaSoltera) {
        listaPersonasSolteras.remove(unaPersonaSoltera);
    }

    public int largoListaPersonasSolteras() {
        return listaPersonasSolteras.size();
    }

    public ArrayList<Persona> getListaPersonasSinPadres() {
        return listaPersonasSinPadres;
    }

    public void setListaPersonasSinPadres(ArrayList<Persona> unaListaPersonasSinPadres) {
        listaPersonasSinPadres = unaListaPersonasSinPadres;
    }

    public void agregarPersonaSinPadres(Persona unaPersonaSinPadres) {
        this.getListaPersonasSinPadres().add(unaPersonaSinPadres);
    }

    public void eliminarPersonaSinPadres(Persona unaPersonaSinPadres) {
        listaPersonasSinPadres.remove(unaPersonaSinPadres);
    }

    public int largoListaPersonasSinPadres() {
        return listaPersonasSinPadres.size();
    }

    public boolean hayRecuerdos() {
        boolean hay = false;
        Persona p;
        for (int i = 0; i < largoListaPersonas() && !hay; i++) {
            p = this.getListaPersonas().get(i);
            if (p.largoListaDocumentos() != 0 || p.largoListaFotos() != 0 || p.largoListaHistorias() != 0) {
                hay = true;
            }
        }
        return hay;
    }

    public Union getPareja(Persona p) {
        for (int i = 0; i < this.getListaUniones().size(); i++) {
            if (this.getListaUniones().get(i).getPersona1().equals(p) || this.getListaUniones().get(i).getPersona2().equals(p)) {
                return this.getListaUniones().get(i);
            }
        }
        return null;
    }

    public Persona getMiembroPareja(Persona p) {
        for (int i = 0; i < largoListaUniones(); i++) {
            Union u = listaUniones.get(i);
            if (u.getPersona1().equals(p)) {
                return u.getPersona2();
            }
            if (u.getPersona2().equals(p)) {
                return u.getPersona1();
            }
        }
        return null;
    }

    public ArrayList<Persona> listaValidadaPersonasSinPadres(Union u) {
        ArrayList<Persona> ret = new ArrayList<>();
        Persona p1 = u.getPersona1();
        Persona p2 = u.getPersona2();
        //VALIDA QUE UNA PAREJA NO PUEDA SER PADRE DE UNO DE LOS INTEGRANTES DE LA PAREJA
        for (int i = 0; i < largoListaPersonasSinPadres(); i++) {
            Persona p = listaPersonasSinPadres.get(i);
            if (!p.equals(p1) && !p.equals(p2)) {
                ret.add(p);
            }
        }
        //VALIDA QUE NO SE PUEDA AGREGAR COMO HIJO DE UNA PAREJA A LA PAREJA DE SU HIJO
        if (u.largoListaHijos() != 0) {
            for (int i = 0; i < ret.size(); i++) {
                Persona p = ret.get(i);
                Persona comp = getMiembroPareja(p);
                if (comp != null) {
                    if (u.getListaHijos().contains(comp)) {
                        ret.remove(p);
                    }
                }
            }
        }
        //VALIDA NO SON QUE UNA PAREJA NO PUEDA SER PADRE DE SUS PADRES
        ArrayList<Persona> padres = new ArrayList<>();
        if (p1.getPadre1() != null) {
            padres.add(p1.getPadre1());
        }
        if (p1.getPadre2() != null) {
            padres.add(p1.getPadre2());
        }
        if (p2.getPadre1() != null) {
            padres.add(p2.getPadre1());
        }
        if (p2.getPadre2() != null) {
            padres.add(p2.getPadre2());
        }
        for (int i = 0; i < padres.size(); i++) {
            Persona p = padres.get(i);
            if (p.getPadre1() != null) {
                padres.add(p.getPadre1());
                padres.add(p.getPadre2());
            }
        }
        for (int i = 0; i < padres.size(); i++) {
            Persona p = padres.get(i);
            if (ret != null) {
                ret.remove(p);
            }
        }

//        //validar para abajo

//        ArrayList<Persona> hijos = new ArrayList<>();
//        for (int i = 0; i < u.largoListaHijos(); i++) {
//            hijos.add(u.getListaHijos().get(i));
//        }
//        for (int i = 0; i < hijos.size(); i++) {
//            Persona p = hijos.get(i);
//            Union uAux = this.getPareja(p);
//            if (uAux != null) {
//                for (int j = 0; j < uAux.largoListaHijos(); j++) {
//                    Persona pAux = uAux.getListaHijos().get(i);
//                    hijos.add(pAux);
//                }
//            }
//        }
//
//        for (int i = 0; i < hijos.size(); i++) {
//            Persona p = hijos.get(i);
//            if (ret != null) {
//                ret.remove(p);
//            }
//        }

        return ret;
    }

    public ArrayList<Persona> personasConRecuerdos() {
        ArrayList<Persona> ret = new ArrayList<>();
        Persona p;
        for (int i = 0; i < largoListaPersonas(); i++) {
            p = this.getListaPersonas().get(i);
            if (p.largoListaDocumentos() != 0 || p.largoListaFotos() != 0 || p.largoListaHistorias() != 0) {
                ret.add(p);
            }
        }
        return ret;
    }

    public ArrayList<Union> getRaicesCandidatas() {
        ArrayList<Union> retorno = new ArrayList();
        Union aux = null;
        for (int i = 0; i < this.largoListaUniones(); i++) {
            aux = this.getListaUniones().get(i);
            if (this.getListaPersonasSinPadres().contains(aux.getPersona1()) && this.getListaPersonasSinPadres().contains(aux.getPersona2())) {
                retorno.add(aux);
            }
        }
        return retorno;
    }

    public boolean sonHermanos(Persona p1, Persona p2) {
        boolean son = false;
        for (int i = 0; i < largoListaUniones() && !son; i++) {
            Union u = listaUniones.get(i);
            if (u.getListaHijos().contains(p1) && u.getListaHijos().contains(p2)) {
                son = true;
            }
        }
        return son;
    }

    public ArrayList<Union> getListaPersonasSolterasConPosiblesHijos() {
        ArrayList<Union> ret = new ArrayList<>();
        for (int i = 0; i < largoListaUniones(); i++) {
            Union u = listaUniones.get(i);
            if (listaValidadaPersonasSinPadres(u).size() != 0) {
                ret.add(u);
            }
        }
        return ret;
    }

    public Persona esParejaDe(Persona p) {
        Union u = this.getPareja(p);
        Persona pareja;
        if (u.getPersona1().equals(p)) {
            pareja = u.getPersona2();
        } else {
            pareja = u.getPersona1();
        }
        return pareja;
    }
}
