/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author victoria
 */
public class Foto implements Serializable {

    private String titulo;
    private String direccion;
    private String fecha;

    public Foto(String titulo, String direccion, String fecha) {
        this.setTitulo(titulo);
        this.setDireccion(direccion);
        this.setFecha(fecha);

    }

    public Foto() {
        this.setTitulo("Sin titulo");
        this.setDireccion("Sin direccion");
        this.setFecha("Sin fecha");
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String unTitulo) {
        titulo = unTitulo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String unaDireccion) {
        direccion = unaDireccion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String unaFecha) {
        fecha = unaFecha;
    }

    @Override
    public String toString() {
        return "Nombre: " + this.getTitulo() + " Fecha: " + this.getFecha();
    }

}
