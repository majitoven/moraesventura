package dominio;

import java.io.Serializable;
import java.util.*;

public class Persona implements Serializable {

    private String nombre;
    private String apellido;
    private String lugarNacimiento;
    private String fechaNacimiento;
    private String nacionalidad;
    private String direccionFoto;
    private Persona padre1;
    private Persona padre2;
    private ArrayList<Foto> listaFotos;
    private ArrayList<Documento> listaDocumentos;
    private ArrayList<Historia> listaHistorias;
    private int sexo;//1.Hombre, 2.Mujer

    public Persona(String nombre, String apellido, String lugarNacimiento, String fechaNacimiento, String nacionalidad, String direccionFoto, ArrayList<Foto> listaFotos, ArrayList<Documento> listaDocumentos, ArrayList<Historia> listaHistorias, int sexo, Persona padre1, Persona padre2) {
        this.setNombre(nombre);
        this.setApellido(apellido);
        this.setLugarNacimiento(lugarNacimiento);
        this.setFechaNacimiento(fechaNacimiento);
        this.setNacionalidad(nacionalidad);
        this.setDireccionFoto(direccionFoto);
        this.setListaFotos(listaFotos);
        this.setListaDocumentos(listaDocumentos);
        this.setListaHistorias(listaHistorias);
        this.setSexo(sexo);
        this.setPadre2(padre1);
        this.setPadre1(padre2);
    }

    public Persona() {
        this.setNombre("Sin nombre");
        this.setApellido("Sin apellido");
        this.setLugarNacimiento("Sin lugar de nacimiento");
        this.setFechaNacimiento("Sin fecha de nacimiento");
        this.setNacionalidad("Sin nacionalidad");
        this.setDireccionFoto("Sin foto");
        listaFotos = new ArrayList<>();
        listaDocumentos = new ArrayList<>();
        listaHistorias = new ArrayList<>();
        this.setSexo(0);
        this.setPadre1(null);
        this.setPadre2(null);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String unNombre) {
        nombre = unNombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String unApellido) {
        apellido = unApellido;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String unLugar) {
        lugarNacimiento = unLugar;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String unaFecha) {
        fechaNacimiento = unaFecha;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String unaNacionalidad) {
        nacionalidad = unaNacionalidad;
    }

    public String getDireccionFoto() {
        return direccionFoto;
    }

    public void setDireccionFoto(String unaDireccionFoto) {
        direccionFoto = unaDireccionFoto;
    }

    public ArrayList<Foto> getListaFotos() {
        return listaFotos;
    }

    public void setListaFotos(ArrayList<Foto> unaListaFotos) {
        listaFotos = unaListaFotos;
    }

    public void agregarFoto(Foto unaFoto) {
        this.getListaFotos().add(unaFoto);
    }

    public void eliminarFoto(Foto unaFoto) {
        this.getListaFotos().remove(unaFoto);
    }

    public int largoListaFotos() {
        return this.getListaFotos().size();
    }

    public ArrayList<Documento> getListaDocumentos() {
        return listaDocumentos;
    }

    public void setListaDocumentos(ArrayList<Documento> unaListaDocumentos) {
        listaDocumentos = unaListaDocumentos;
    }

    public void agregarDocumento(Documento unDocumento) {
        this.getListaDocumentos().add(unDocumento);
    }

    public void eliminarDocumento(Documento unDocumento) {
        this.getListaDocumentos().remove(unDocumento);
    }

    public int largoListaDocumentos() {
        return this.getListaDocumentos().size();
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int unSexo) {
        sexo = unSexo;
    }

    public ArrayList<Historia> getListaHistorias() {
        return listaHistorias;
    }

    public void setListaHistorias(ArrayList<Historia> unaListaHistorias) {
        listaHistorias = unaListaHistorias;
    }

    public void agregarHistoria(Historia unaHistoria) {
        this.getListaHistorias().add(unaHistoria);
    }

    public void eliminarHistoria(Historia unaHistoria) {
        this.getListaHistorias().remove(unaHistoria);
    }

    public int largoListaHistorias() {
        return this.getListaHistorias().size();
    }

    public Persona getPadre1() {
        return padre1;
    }

    public void setPadre1(Persona unPadre1) {
        padre1 = unPadre1;
    }

    public Persona getPadre2() {
        return padre2;
    }

    public void setPadre2(Persona unPadre2) {
        padre2 = unPadre2;
    }

    @Override
    public boolean equals(Object o) {
        Persona p = (Persona) o;
        return (this.getNombre().equalsIgnoreCase(p.getNombre()) && (this.getApellido().equalsIgnoreCase(p.getApellido())) && (this.getFechaNacimiento().equalsIgnoreCase(p.getFechaNacimiento())));
    }

    @Override
    public String toString() {
        return this.getNombre() + " " + this.getApellido();
    }

}
