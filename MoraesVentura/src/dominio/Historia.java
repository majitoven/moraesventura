
package dominio;

import java.io.Serializable;
import java.util.ArrayList;


public class Historia implements Serializable {

    private String titulo;
    private String contenido;
    private String fecha;

    public Historia(String titulo, String contenido, String fecha) {
        this.setTitulo(titulo);
        this.setContenido(contenido);
        this.setFecha(fecha);

    }

    public Historia() {
        this.setTitulo("Sin titulo");
        this.setContenido("Sin contenido");
        this.setFecha("Sin fecha");
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String unTitulo) {
        titulo = unTitulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String unContenido) {
        contenido = unContenido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String unaFecha) {
        fecha = unaFecha;
    }

    @Override
    public String toString() {
        return "Nombre: " + this.getTitulo() + " Fecha: " + this.getFecha();
    }

}
