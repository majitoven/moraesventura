package interfaz;

import dominio.*;
import javax.swing.*;

public class Inicio extends JPanel {

    private JPanel padre;
    private Sistema sistema;


    public Inicio(JPanel panelPadre, Sistema sis) {
        
        padre = panelPadre;
        sistema = sis;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        botonComenzar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(1024, 768));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        botonComenzar.setBackground(new java.awt.Color(0, 153, 153));
        botonComenzar.setBorderPainted(false);
        botonComenzar.setContentAreaFilled(false);
        botonComenzar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        botonComenzar.setName("botonComenzar"); // NOI18N
        botonComenzar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonComenzarActionPerformed(evt);
            }
        });
        add(botonComenzar, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 510, 320, 80));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/inicio.png"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void botonComenzarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonComenzarActionPerformed
        padre.removeAll();
        padre.add(new Arbol(padre, sistema,null));
        padre.updateUI();
    }//GEN-LAST:event_botonComenzarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonComenzar;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
