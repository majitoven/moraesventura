package interfaz;

import java.util.*;
import dominio.*;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class AgregarPareja extends javax.swing.JPanel {

    private Sistema sistema;
    private JPanel padre;

    public AgregarPareja(JPanel panelPadre, Sistema sis) {

        sistema = sis;
        initComponents();
        padre = panelPadre;

        for (int i = 1800; i <= 2017; i++) {
            String s = Integer.toString(i);
            comboBoxAñoI.addItem(s);
            comboBoxAñoF.addItem(s);
        }
        comboBoxAñoI.setSelectedIndex(0);
        comboBoxAñoF.setSelectedIndex(0);
        mostrarListas();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupTipo = new javax.swing.ButtonGroup();
        btnVolver = new javax.swing.JButton();
        btnAceptar = new javax.swing.JButton();
        comboBoxTipo = new javax.swing.JComboBox<>();
        lblFechaInicio = new javax.swing.JLabel();
        lblFechaFinalizacion = new javax.swing.JLabel();
        comboBoxMesI = new javax.swing.JComboBox<>();
        lblCtrl = new javax.swing.JLabel();
        comboBoxMesF = new javax.swing.JComboBox<>();
        comboBoxAñoF = new javax.swing.JComboBox<>();
        comboBoxDiaI = new javax.swing.JComboBox<>();
        comboBoxAñoI = new javax.swing.JComboBox<>();
        comboBoxDiaF = new javax.swing.JComboBox<>();
        lblTipo = new javax.swing.JLabel();
        checkBoxFin = new javax.swing.JCheckBox();
        jScrollPanePersonas = new javax.swing.JScrollPane();
        listaPersonas = new javax.swing.JList();
        lblcheckbox = new javax.swing.JLabel();
        labelFondo = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnVolver.setBorderPainted(false);
        btnVolver.setContentAreaFilled(false);
        btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });
        add(btnVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 700, 150, 50));

        btnAceptar.setBorderPainted(false);
        btnAceptar.setContentAreaFilled(false);
        btnAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        add(btnAceptar, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 710, 140, 40));

        comboBoxTipo.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        comboBoxTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Matrimonio", "Concubinato" }));
        comboBoxTipo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        add(comboBoxTipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 500, 140, -1));

        lblFechaInicio.setFont(new java.awt.Font("Century Gothic", 1, 16)); // NOI18N
        lblFechaInicio.setForeground(new java.awt.Color(255, 255, 255));
        lblFechaInicio.setText("Fecha de inicio:");
        add(lblFechaInicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 540, -1, -1));

        lblFechaFinalizacion.setFont(new java.awt.Font("Century Gothic", 1, 16)); // NOI18N
        lblFechaFinalizacion.setForeground(new java.awt.Color(255, 255, 255));
        lblFechaFinalizacion.setText("Fecha de finalización:");
        add(lblFechaFinalizacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 580, -1, -1));

        comboBoxMesI.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        comboBoxMesI.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        comboBoxMesI.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        comboBoxMesI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxMesIActionPerformed(evt);
            }
        });
        add(comboBoxMesI, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 540, 50, -1));

        lblCtrl.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        lblCtrl.setForeground(new java.awt.Color(255, 255, 255));
        lblCtrl.setText("*Presione \"Ctrl\" para seleccionar más de una persona.");
        add(lblCtrl, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 430, 360, -1));

        comboBoxMesF.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        comboBoxMesF.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        comboBoxMesF.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        comboBoxMesF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxMesFActionPerformed(evt);
            }
        });
        add(comboBoxMesF, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 580, 50, -1));

        comboBoxAñoF.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        comboBoxAñoF.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        add(comboBoxAñoF, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 580, 70, -1));

        comboBoxDiaI.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        comboBoxDiaI.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        comboBoxDiaI.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        add(comboBoxDiaI, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 540, 50, -1));

        comboBoxAñoI.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        comboBoxAñoI.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        add(comboBoxAñoI, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 540, 70, -1));

        comboBoxDiaF.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        comboBoxDiaF.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        comboBoxDiaF.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        comboBoxDiaF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxDiaFActionPerformed(evt);
            }
        });
        add(comboBoxDiaF, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 580, 50, -1));

        lblTipo.setFont(new java.awt.Font("Century Gothic", 1, 16)); // NOI18N
        lblTipo.setForeground(new java.awt.Color(255, 255, 255));
        lblTipo.setText("Tipo:");
        add(lblTipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 500, -1, -1));

        checkBoxFin.setFont(new java.awt.Font("Avenir", 1, 13)); // NOI18N
        checkBoxFin.setForeground(new java.awt.Color(255, 255, 255));
        checkBoxFin.setContentAreaFilled(false);
        checkBoxFin.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        checkBoxFin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkBoxFinActionPerformed(evt);
            }
        });
        add(checkBoxFin, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 620, -1, -1));

        jScrollPanePersonas.setBorder(null);

        listaPersonas.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        listaPersonas.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        listaPersonas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        listaPersonas.setRequestFocusEnabled(false);
        listaPersonas.setSelectionBackground(new java.awt.Color(0, 153, 153));
        listaPersonas.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listaPersonasValueChanged(evt);
            }
        });
        jScrollPanePersonas.setViewportView(listaPersonas);

        add(jScrollPanePersonas, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 140, 370, 280));

        lblcheckbox.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        lblcheckbox.setForeground(new java.awt.Color(255, 255, 255));
        lblcheckbox.setText("La unión no ha finalizado.");
        add(lblcheckbox, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 624, -1, -1));

        labelFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/agregarPareja.png"))); // NOI18N
        labelFondo.setToolTipText("");
        add(labelFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        padre.removeAll();
        padre.add(new AgregarRelacion(padre, sistema));
        padre.updateUI();
    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        if (listaPersonas.getSelectedValuesList().size() == 2) {
            Union u = new Union();
            List l = listaPersonas.getSelectedValuesList();
            Persona p1 = (Persona) l.get(0);
            Persona p2 = (Persona) l.get(1);
            if (!sistema.sonHermanos(p1, p2)) {
                u.setPersona1(p1);
                u.setPersona2(p2);
                String fechaStringI = ((comboBoxDiaI.getSelectedItem()).toString() + "/" + (comboBoxMesI.getSelectedItem()).toString() + "/" + (comboBoxAñoI.getSelectedItem()).toString());
                u.setFechaInicio(fechaStringI);
                if (!checkBoxFin.isSelected()) {
                    String fechaStringF = ((comboBoxDiaF.getSelectedItem()).toString() + "/" + (comboBoxMesF.getSelectedItem()).toString() + "/" + (comboBoxAñoF.getSelectedItem()).toString());
                    u.setFechaFin(fechaStringF);
                }
                u.setTipo(comboBoxTipo.getSelectedIndex());
                sistema.eliminarPersonaSoltera(p1);
                sistema.eliminarPersonaSoltera(p2);
                sistema.agregarUnion(u);
                JOptionPane.showMessageDialog(padre, "Registro exitoso", "Agregar pareja", 1);
                padre.removeAll();
                padre.add(new AgregarRelacion(padre, sistema));
                padre.updateUI();
            } else {
                JOptionPane.showMessageDialog(padre, "Dos hermanos/as no pueden ser pareja.", "Error", 0);

            }
        } else {
            JOptionPane.showMessageDialog(padre, "Deben haber dos personas seleccionadas.", "Error", 0);
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void checkBoxFinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkBoxFinActionPerformed

        if (checkBoxFin.isSelected()) {
            comboBoxDiaF.setEnabled(false);
            comboBoxMesF.setEnabled(false);
            comboBoxAñoF.setEnabled(false);
        } else {
            comboBoxDiaF.setEnabled(true);
            comboBoxMesF.setEnabled(true);
            comboBoxAñoF.setEnabled(true);
        }

    }//GEN-LAST:event_checkBoxFinActionPerformed

    private void comboBoxMesIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxMesIActionPerformed
        comboBoxDiaI.removeAllItems();
        if (comboBoxMesI.getSelectedIndex() == 0 || comboBoxMesI.getSelectedIndex() == 2 || comboBoxMesI.getSelectedIndex() == 4 || comboBoxMesI.getSelectedIndex() == 6 || comboBoxMesI.getSelectedIndex() == 7 || comboBoxMesI.getSelectedIndex() == 9 || comboBoxMesI.getSelectedIndex() == 11) {
            for (int i = 1; i <= 31; i++) {
                String s = Integer.toString(i);
                comboBoxDiaI.addItem(s);
            }
        }
        if (comboBoxMesI.getSelectedIndex() == 1) {
            for (int i = 1; i <= 29; i++) {
                String s = Integer.toString(i);
                comboBoxDiaI.addItem(s);
            }
        }
        if (comboBoxMesI.getSelectedIndex() == 3 || comboBoxMesI.getSelectedIndex() == 5 || comboBoxMesI.getSelectedIndex() == 8 || comboBoxMesI.getSelectedIndex() == 10) {
            for (int i = 1; i <= 30; i++) {
                String s = Integer.toString(i);
                comboBoxDiaI.addItem(s);
            }
        }    }//GEN-LAST:event_comboBoxMesIActionPerformed

    private void comboBoxMesFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxMesFActionPerformed
        comboBoxDiaF.removeAllItems();
        if (comboBoxMesF.getSelectedIndex() == 0 || comboBoxMesF.getSelectedIndex() == 2 || comboBoxMesF.getSelectedIndex() == 4 || comboBoxMesF.getSelectedIndex() == 6 || comboBoxMesF.getSelectedIndex() == 7 || comboBoxMesF.getSelectedIndex() == 9 || comboBoxMesF.getSelectedIndex() == 11) {
            for (int i = 1; i <= 31; i++) {
                String s = Integer.toString(i);
                comboBoxDiaF.addItem(s);
            }
        }
        if (comboBoxMesI.getSelectedIndex() == 1) {
            for (int i = 1; i <= 29; i++) {
                String s = Integer.toString(i);
                comboBoxDiaF.addItem(s);
            }
        }
        if (comboBoxMesF.getSelectedIndex() == 3 || comboBoxMesF.getSelectedIndex() == 5 || comboBoxMesF.getSelectedIndex() == 8 || comboBoxMesF.getSelectedIndex() == 10) {
            for (int i = 1; i <= 30; i++) {
                String s = Integer.toString(i);
                comboBoxDiaF.addItem(s);
            }
        }
    }//GEN-LAST:event_comboBoxMesFActionPerformed

    private void listaPersonasValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listaPersonasValueChanged

    }//GEN-LAST:event_listaPersonasValueChanged

    private void comboBoxDiaFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxDiaFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboBoxDiaFActionPerformed

    private void mostrarListas() {
        listaPersonas.setListData(sistema.getListaPersonasSolteras().toArray());
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnVolver;
    private javax.swing.ButtonGroup buttonGroupTipo;
    private javax.swing.JCheckBox checkBoxFin;
    private javax.swing.JComboBox<String> comboBoxAñoF;
    private javax.swing.JComboBox<String> comboBoxAñoI;
    private javax.swing.JComboBox<String> comboBoxDiaF;
    private javax.swing.JComboBox<String> comboBoxDiaI;
    private javax.swing.JComboBox<String> comboBoxMesF;
    private javax.swing.JComboBox<String> comboBoxMesI;
    private javax.swing.JComboBox<String> comboBoxTipo;
    private javax.swing.JScrollPane jScrollPanePersonas;
    private javax.swing.JLabel labelFondo;
    private javax.swing.JLabel lblCtrl;
    private javax.swing.JLabel lblFechaFinalizacion;
    private javax.swing.JLabel lblFechaInicio;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JLabel lblcheckbox;
    private javax.swing.JList listaPersonas;
    // End of variables declaration//GEN-END:variables
}
