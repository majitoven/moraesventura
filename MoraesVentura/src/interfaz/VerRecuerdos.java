package interfaz;

import dominio.Documento;
import dominio.Foto;
import dominio.Historia;
import dominio.Persona;
import dominio.Sistema;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author mariaventura
 */
public class VerRecuerdos extends javax.swing.JPanel {

    private Sistema sistema;
    private JPanel padre;
    private Persona persona;

    public VerRecuerdos(JPanel panelPadre, Sistema sis, Persona p) {
        sistema = sis;
        padre = panelPadre;
        persona = p;
        initComponents();
        personalizarListas();
        cargarListas(p);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoRecuerdos = new javax.swing.ButtonGroup();
        scrollPaneDocumentos = new javax.swing.JScrollPane();
        listaDocumentos = new javax.swing.JList();
        scrollPaneFotos = new javax.swing.JScrollPane();
        listaFotos = new javax.swing.JList();
        btnVolver = new javax.swing.JButton();
        scrollPaneHistorias = new javax.swing.JScrollPane();
        listaHistorias = new javax.swing.JList();
        btnAceptar = new javax.swing.JButton();
        lblFondo = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        scrollPaneDocumentos.setBorder(null);

        listaDocumentos.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        listaDocumentos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listaDocumentos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        listaDocumentos.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listaDocumentosValueChanged(evt);
            }
        });
        scrollPaneDocumentos.setViewportView(listaDocumentos);

        add(scrollPaneDocumentos, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 220, 270, 380));

        scrollPaneFotos.setBorder(null);

        listaFotos.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        listaFotos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listaFotos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        listaFotos.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listaFotosValueChanged(evt);
            }
        });
        scrollPaneFotos.setViewportView(listaFotos);

        add(scrollPaneFotos, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 220, 270, 380));

        btnVolver.setBorderPainted(false);
        btnVolver.setContentAreaFilled(false);
        btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });
        add(btnVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 700, 160, 50));

        scrollPaneHistorias.setBorder(null);

        listaHistorias.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        listaHistorias.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listaHistorias.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        listaHistorias.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listaHistoriasValueChanged(evt);
            }
        });
        scrollPaneHistorias.setViewportView(listaHistorias);

        add(scrollPaneHistorias, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 220, 270, 380));

        btnAceptar.setBorderPainted(false);
        btnAceptar.setContentAreaFilled(false);
        btnAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        add(btnAceptar, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 690, 140, 50));

        lblFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/verRecuerdos.png"))); // NOI18N
        add(lblFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        padre.removeAll();
        padre.add(new VerRecuerdosDe(padre, sistema));
        padre.updateUI();
    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed

        if (!listaHistorias.isSelectionEmpty()) {
            Historia h = (Historia) listaHistorias.getSelectedValue();
            padre.removeAll();
            padre.add(new VerHistoria(padre, sistema, h, persona));
            padre.updateUI();
        } else if (!listaFotos.isSelectionEmpty()) {
            Foto f = (Foto) listaFotos.getSelectedValue();
            padre.removeAll();
            padre.add(new VerFoto(padre, sistema, f.getDireccion(), persona));
            padre.updateUI();
        } else if (!listaDocumentos.isSelectionEmpty()) {
            Documento d = (Documento) listaDocumentos.getSelectedValue();
            File archivoDestino = new File(d.getDireccion());
            try {
                Desktop.getDesktop().open(archivoDestino);
            } catch (IOException ex) {
                Logger.getLogger(VerRecuerdos.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(padre, "Debe seleccionar algun recuerdo de la lista.", "Error", 0);
        }


    }//GEN-LAST:event_btnAceptarActionPerformed

    private void listaHistoriasValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listaHistoriasValueChanged
        habilitarListaHistorias();
    }//GEN-LAST:event_listaHistoriasValueChanged

    private void listaFotosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listaFotosValueChanged
        habilitarListaFotos();
    }//GEN-LAST:event_listaFotosValueChanged

    private void listaDocumentosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listaDocumentosValueChanged
        habilitarListaDocumentos();
    }//GEN-LAST:event_listaDocumentosValueChanged

    public void cargarListas(Persona p) {
        if (p.largoListaDocumentos() != 0) {
            listaDocumentos.setListData(p.getListaDocumentos().toArray());
        } else {
            listaDocumentos.setEnabled(false);
        }
        if (p.largoListaFotos() != 0) {
            listaFotos.setListData(p.getListaFotos().toArray());
        } else {
            listaFotos.setEnabled(false);
        }
        if (p.largoListaHistorias() != 0) {
            listaHistorias.setListData(p.getListaHistorias().toArray());
        } else {
            listaDocumentos.setEnabled(true);
            habilitarListaDocumentos();
        }

    }

    public void personalizarListas() {
        scrollPaneDocumentos.setOpaque(false);
        scrollPaneDocumentos.getViewport().setOpaque(false);
        listaFotos.setOpaque(false);
        listaHistorias.setOpaque(false);
        listaDocumentos.setOpaque(false);
        scrollPaneHistorias.setOpaque(false);
        scrollPaneHistorias.getViewport().setOpaque(false);
        scrollPaneFotos.setOpaque(false);
        scrollPaneFotos.getViewport().setOpaque(false);
    }

    public void habilitarListaFotos() {
        listaHistorias.clearSelection();
        listaDocumentos.clearSelection();
    }

    public void habilitarListaHistorias() {
        listaFotos.clearSelection();
        listaDocumentos.clearSelection();
    }

    public void habilitarListaDocumentos() {
        listaHistorias.clearSelection();
        listaFotos.clearSelection();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnVolver;
    private javax.swing.ButtonGroup grupoRecuerdos;
    private javax.swing.JLabel lblFondo;
    private javax.swing.JList listaDocumentos;
    private javax.swing.JList listaFotos;
    private javax.swing.JList listaHistorias;
    private javax.swing.JScrollPane scrollPaneDocumentos;
    private javax.swing.JScrollPane scrollPaneFotos;
    private javax.swing.JScrollPane scrollPaneHistorias;
    // End of variables declaration//GEN-END:variables
}
