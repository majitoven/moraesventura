package interfaz;

import dominio.*;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author mariaventura
 */
public class AgregarRecuerdo extends javax.swing.JPanel {

    private Sistema sistema;
    private JPanel padre;
    private String direccionFoto;
    private String direccionDocumento;
    private String tituloFoto;
    private String tituloDocumento;
    private FileNameExtensionFilter filter;
    private FileNameExtensionFilter filterDoc;
    private Historia historia;

    public AgregarRecuerdo(JPanel panelPadre, Sistema sis, Historia h) {
        sistema = sis;
        padre = panelPadre;
        direccionFoto = "Sin foto";
        direccionDocumento = "Sin documento";
        tituloFoto = "Sin titulo";
        tituloDocumento = "Sin titulo";
        historia = h;
        initComponents();
        filter = new FileNameExtensionFilter("Archivo de imagen", "png", "jpg", "jpeg");
        filterDoc = new FileNameExtensionFilter("Archivo de texto", "doc", "pdf", "txt", "docx", "pages");
        jListPersonas.setListData(sistema.getListaPersonas().toArray());
        for (int i = 1800; i <= 2017; i++) {
            String s = Integer.toString(i);
            comboBoxAño.addItem(s);
        }
        comboBoxAño.setSelectedIndex(0);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnHistoria = new javax.swing.JButton();
        btnFoto = new javax.swing.JButton();
        btnDocumento = new javax.swing.JButton();
        btnVolver = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnAceptar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListPersonas = new javax.swing.JList();
        comboBoxDia = new javax.swing.JComboBox<>();
        comboBoxMes = new javax.swing.JComboBox<>();
        comboBoxAño = new javax.swing.JComboBox<>();
        lblSugerencia = new javax.swing.JLabel();
        lblFondo = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnHistoria.setBorderPainted(false);
        btnHistoria.setContentAreaFilled(false);
        btnHistoria.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnHistoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHistoriaActionPerformed(evt);
            }
        });
        add(btnHistoria, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 260, 340, 50));

        btnFoto.setBorderPainted(false);
        btnFoto.setContentAreaFilled(false);
        btnFoto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFotoActionPerformed(evt);
            }
        });
        add(btnFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 340, 340, 50));

        btnDocumento.setBorderPainted(false);
        btnDocumento.setContentAreaFilled(false);
        btnDocumento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDocumentoActionPerformed(evt);
            }
        });
        add(btnDocumento, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 420, 340, 50));

        btnVolver.setBorderPainted(false);
        btnVolver.setContentAreaFilled(false);
        btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });
        add(btnVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 700, 150, 50));

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Fecha:");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 160, -1, -1));

        btnAceptar.setBorderPainted(false);
        btnAceptar.setContentAreaFilled(false);
        btnAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        add(btnAceptar, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 710, 140, 40));

        jListPersonas.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jListPersonas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jScrollPane1.setViewportView(jListPersonas);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 240, 280, 300));

        comboBoxDia.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        comboBoxDia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        add(comboBoxDia, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 160, 50, -1));

        comboBoxMes.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        comboBoxMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        comboBoxMes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxMesActionPerformed(evt);
            }
        });
        add(comboBoxMes, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 160, 50, -1));

        comboBoxAño.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        add(comboBoxAño, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 160, 70, -1));

        lblSugerencia.setFont(new java.awt.Font("Avenir", 0, 14)); // NOI18N
        lblSugerencia.setForeground(new java.awt.Color(255, 255, 255));
        lblSugerencia.setText("*Presione \"Ctrl\" para seleccionar más de una persona.");
        add(lblSugerencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 550, -1, -1));

        lblFondo.setForeground(new java.awt.Color(0, 51, 51));
        lblFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/agregarRecuerdo.png"))); // NOI18N
        add(lblFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnHistoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHistoriaActionPerformed
        padre.removeAll();
        padre.add(new AgregarHistoria(padre, sistema));
        padre.updateUI();

    }//GEN-LAST:event_btnHistoriaActionPerformed

    private void btnFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFotoActionPerformed
        JFileChooser objeto = new JFileChooser();
        objeto.setFileFilter(filter);
        int opcion = objeto.showOpenDialog(this);
        if (opcion == JFileChooser.APPROVE_OPTION) {
            File archivo = objeto.getSelectedFile();
            try {
                FileInputStream inStream = new FileInputStream(archivo);
                File archivoDestino = new File(System.getProperty("user.dir") + "/src/usuarios", archivo.getName());
                tituloFoto = archivo.getName();
                direccionFoto = System.getProperty("user.dir") + "/src/usuarios/" + archivo.getName();
                FileOutputStream outStream = new FileOutputStream(archivoDestino);
                byte[] buffer = new byte[1024];
                int largo;
                while ((largo = inStream.read(buffer)) > 0) {
                    outStream.write(buffer, 0, largo);
                }
            } catch (IOException ex) {
                //mostrarJOptionPane
            }
        }
    }//GEN-LAST:event_btnFotoActionPerformed

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        padre.removeAll();
        padre.add(new Arbol(padre, sistema, null));
        padre.updateUI();
    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed

        if (!jListPersonas.isSelectionEmpty()) {
            if (direccionFoto.equals("Sin foto") && direccionDocumento.equals("Sin documento") && historia == null) {
                JOptionPane.showMessageDialog(padre, "Debe subir algun recuerdo", "Error", 0);
            } else if (!direccionFoto.equals("Sin foto") || !direccionDocumento.equals("Sin documento") || historia != null) {
                List<Persona> aux = new ArrayList<Persona>();
                aux = jListPersonas.getSelectedValuesList();
                String fecha = ((comboBoxDia.getSelectedItem()).toString() + "/" + (comboBoxMes.getSelectedItem()).toString() + "/" + (comboBoxAño.getSelectedItem()).toString());
                for (int i = 0; i < aux.size(); i++) {
                    if (!direccionFoto.equals("Sin foto")) {
                        Foto f = new Foto(tituloFoto, direccionFoto, fecha);
                        aux.get(i).agregarFoto(f);
                    }
                    if (!direccionDocumento.equals("Sin documento")) {
                        Documento d = new Documento(tituloDocumento, direccionDocumento, fecha);
                        aux.get(i).agregarDocumento(d);
                    }
                    if (historia != null) {
                        historia.setFecha(fecha);
                        aux.get(i).agregarHistoria(historia);
                    }
                }
                JOptionPane.showMessageDialog(padre, "Los recuerdos se han registrado con exito.", "Agregar recuerdos", 1);
                direccionFoto = "Sin foto";
                direccionDocumento = "Sin documento";
                historia = null;
                padre.removeAll();
                padre.add(new Arbol(padre, sistema, null));
                padre.updateUI();
            }

        } else {
            JOptionPane.showMessageDialog(padre, "Debe seleccionar alguna persona", "Error", 0);
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void btnDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDocumentoActionPerformed
        JFileChooser objeto = new JFileChooser();
        objeto.setFileFilter(filterDoc);
        int opcion = objeto.showOpenDialog(this);
        if (opcion == JFileChooser.APPROVE_OPTION) {
            File archivo = objeto.getSelectedFile();
            try {
                FileInputStream inStream = new FileInputStream(archivo);
                File archivoDestino = new File(System.getProperty("user.dir") + "/src/usuarios", archivo.getName());
                tituloDocumento = archivo.getName();
                direccionDocumento = System.getProperty("user.dir") + "/src/usuarios/" + archivo.getName();
                FileOutputStream outStream = new FileOutputStream(archivoDestino);
                byte[] buffer = new byte[1024];
                int largo;
                while ((largo = inStream.read(buffer)) > 0) {
                    outStream.write(buffer, 0, largo);
                }
            } catch (IOException ex) {
                //mostrarJOptionPane
            }
        }
    }//GEN-LAST:event_btnDocumentoActionPerformed

    private void comboBoxMesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxMesActionPerformed
        comboBoxDia.removeAllItems();
        if (comboBoxMes.getSelectedIndex() == 0 || comboBoxMes.getSelectedIndex() == 2 || comboBoxMes.getSelectedIndex() == 4 || comboBoxMes.getSelectedIndex() == 6 || comboBoxMes.getSelectedIndex() == 7 || comboBoxMes.getSelectedIndex() == 9 || comboBoxMes.getSelectedIndex() == 11) {

            for (int i = 1; i <= 31; i++) {
                String s = Integer.toString(i);
                comboBoxDia.addItem(s);
            }
        }
        if (comboBoxMes.getSelectedIndex() == 1) {
            for (int i = 1; i <= 29; i++) {
                String s = Integer.toString(i);
                comboBoxDia.addItem(s);
            }
        }
        if (comboBoxMes.getSelectedIndex() == 3 || comboBoxMes.getSelectedIndex() == 5 || comboBoxMes.getSelectedIndex() == 8 || comboBoxMes.getSelectedIndex() == 10) {
            for (int i = 1; i <= 30; i++) {
                String s = Integer.toString(i);
                comboBoxDia.addItem(s);
            }
        }
    }//GEN-LAST:event_comboBoxMesActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnDocumento;
    private javax.swing.JButton btnFoto;
    private javax.swing.JButton btnHistoria;
    private javax.swing.JButton btnVolver;
    private javax.swing.JComboBox<String> comboBoxAño;
    private javax.swing.JComboBox<String> comboBoxDia;
    private javax.swing.JComboBox<String> comboBoxMes;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JList jListPersonas;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblFondo;
    private javax.swing.JLabel lblSugerencia;
    // End of variables declaration//GEN-END:variables
}
