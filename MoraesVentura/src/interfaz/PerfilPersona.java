package interfaz;

import dominio.*;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

public class PerfilPersona extends javax.swing.JPanel {

    private Sistema sistema;
    private JPanel padre;
    private Persona persona;
    private ImageIcon fotoPerfil;
    private Union union;

    public PerfilPersona(JPanel panelPadre, Sistema sis, Persona p, Union u) {
        union = u;
        persona = p;
        sistema = sis;
        padre = panelPadre;
        initComponents();
        setVisible(false);
        cargarPerfil();
    }

    public void cargarPerfil() {
        cargarFoto(persona);
        lblNombre.setText(persona.getNombre());
        lblApellido.setText(persona.getApellido());
        lblFechaNac.setText(persona.getFechaNacimiento());
        lblLugarNac.setText(persona.getLugarNacimiento());
        lblNacionalidad.setText(persona.getNacionalidad());
        if (sistema.getPareja(persona) != null) {
            setVisible(true);
            Union estaPareja=sistema.getPareja(persona);
            btnPareja.setText(sistema.esParejaDe(persona).toString());
            lblDesde.setText(estaPareja.getFechaInicio());
            if(estaPareja.getFechaFin().equals("Sin fecha")){
                lblHasta.setVisible(false);
                lblTituloHasta.setVisible(false);
            }
            else{
                lblHasta.setText(estaPareja.getFechaFin());
            }
        }
    }
    
   

    public void cargarFoto(Persona p) {
        String dir = p.getDireccionFoto();
        if (dir.equals("Sin foto")) {
            if (p.getSexo() == 1) {
                dir = System.getProperty("user.dir") + "/src/recursos/" + "fotoHombre.png";
            } else {
                dir = System.getProperty("user.dir") + "/src/recursos/" + "fotoMujer.png";
            }
        } else {
            dir = p.getDireccionFoto();
        }
        fotoPerfil = new ImageIcon(dir);
        Image img = fotoPerfil.getImage();
        Image newimg = img.getScaledInstance(132, 149, java.awt.Image.SCALE_SMOOTH);
        fotoPerfil = new ImageIcon(newimg);
        lblFotoPerfil.setIcon(fotoPerfil);
    }

    public void setVisible(boolean estado) {
        lblPareja.setVisible(estado);
        btnPareja.setVisible(estado);
        lblSugerencia.setVisible(estado);
        lblDesde.setVisible(estado);
        lblHasta.setVisible(estado);
        lblTituloDesde.setVisible(estado);
        lblTituloHasta.setVisible(estado);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblNombre = new javax.swing.JLabel();
        lblApellido = new javax.swing.JLabel();
        lblFechaNac = new javax.swing.JLabel();
        lblLugarNac = new javax.swing.JLabel();
        lblNacionalidad = new javax.swing.JLabel();
        btnVolver = new javax.swing.JButton();
        lblPareja = new javax.swing.JLabel();
        btnPareja = new javax.swing.JButton();
        lblSugerencia = new javax.swing.JLabel();
        lblFotoPerfil = new javax.swing.JLabel();
        lblTituloDesde = new javax.swing.JLabel();
        lblDesde = new javax.swing.JLabel();
        lblHasta = new javax.swing.JLabel();
        lblTituloHasta = new javax.swing.JLabel();
        labelFondo = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblNombre.setFont(new java.awt.Font("Century Gothic", 0, 25)); // NOI18N
        lblNombre.setForeground(new java.awt.Color(141, 137, 101));
        add(lblNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 260, 510, 50));

        lblApellido.setFont(new java.awt.Font("Century Gothic", 0, 25)); // NOI18N
        lblApellido.setForeground(new java.awt.Color(141, 137, 101));
        add(lblApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 330, 510, 50));

        lblFechaNac.setFont(new java.awt.Font("Century Gothic", 0, 25)); // NOI18N
        lblFechaNac.setForeground(new java.awt.Color(141, 137, 101));
        add(lblFechaNac, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 400, 510, 50));

        lblLugarNac.setFont(new java.awt.Font("Century Gothic", 0, 25)); // NOI18N
        lblLugarNac.setForeground(new java.awt.Color(141, 137, 101));
        add(lblLugarNac, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 470, 510, 50));

        lblNacionalidad.setFont(new java.awt.Font("Century Gothic", 0, 25)); // NOI18N
        lblNacionalidad.setForeground(new java.awt.Color(141, 137, 101));
        add(lblNacionalidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 540, 510, 50));

        btnVolver.setBorderPainted(false);
        btnVolver.setContentAreaFilled(false);
        btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });
        add(btnVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 660, 160, 40));

        lblPareja.setFont(new java.awt.Font("Century Gothic", 0, 22)); // NOI18N
        lblPareja.setForeground(new java.awt.Color(0, 102, 102));
        lblPareja.setText("En pareja con:");
        add(lblPareja, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 110, 160, 30));

        btnPareja.setBackground(new java.awt.Color(192, 223, 223));
        btnPareja.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        btnPareja.setForeground(new java.awt.Color(255, 255, 255));
        btnPareja.setBorderPainted(false);
        btnPareja.setContentAreaFilled(false);
        btnPareja.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPareja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnParejaActionPerformed(evt);
            }
        });
        add(btnPareja, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 150, 330, 40));

        lblSugerencia.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        lblSugerencia.setForeground(new java.awt.Color(0, 153, 153));
        lblSugerencia.setText("(Ver perfil)");
        add(lblSugerencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 180, -1, -1));
        add(lblFotoPerfil, new org.netbeans.lib.awtextra.AbsoluteConstraints(508, 80, 133, 150));

        lblTituloDesde.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        lblTituloDesde.setForeground(new java.awt.Color(0, 102, 102));
        lblTituloDesde.setText("Desde:");
        add(lblTituloDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 200, -1, -1));

        lblDesde.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        lblDesde.setForeground(new java.awt.Color(0, 102, 102));
        add(lblDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 200, -1, -1));

        lblHasta.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        lblHasta.setForeground(new java.awt.Color(0, 102, 102));
        add(lblHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 230, -1, -1));

        lblTituloHasta.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        lblTituloHasta.setForeground(new java.awt.Color(0, 102, 102));
        lblTituloHasta.setText("Hasta:");
        add(lblTituloHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 230, -1, -1));

        labelFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/perfil.png"))); // NOI18N
        add(labelFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        padre.removeAll();
        padre.add(new Arbol(padre, sistema, union));
        padre.updateUI();
    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnParejaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnParejaActionPerformed
        padre.removeAll();
        padre.add(new PerfilPersona(padre, sistema, sistema.esParejaDe(persona), union));
        padre.updateUI();
    }//GEN-LAST:event_btnParejaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPareja;
    private javax.swing.JButton btnVolver;
    private javax.swing.JLabel labelFondo;
    private javax.swing.JLabel lblApellido;
    private javax.swing.JLabel lblDesde;
    private javax.swing.JLabel lblFechaNac;
    private javax.swing.JLabel lblFotoPerfil;
    private javax.swing.JLabel lblHasta;
    private javax.swing.JLabel lblLugarNac;
    private javax.swing.JLabel lblNacionalidad;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblPareja;
    private javax.swing.JLabel lblSugerencia;
    private javax.swing.JLabel lblTituloDesde;
    private javax.swing.JLabel lblTituloHasta;
    // End of variables declaration//GEN-END:variables
}
