package interfaz;

import dominio.*;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class RegistrarPersona extends javax.swing.JPanel {

    private Sistema sistema;
    private JPanel padre;
    private FileNameExtensionFilter filter;
    private ImageIcon fotoPerfil;
    private String direccionFotoPerfil;

    public RegistrarPersona(JPanel panelPadre, Sistema sis) {

        sistema = sis;
        initComponents();
        padre = panelPadre;
        direccionFotoPerfil = "Sin foto";
        filter = new FileNameExtensionFilter("Archivo de imagen", "png", "jpg", "jpeg");
        fotoPerfil = new ImageIcon(getClass().getResource("/recursos/subirFoto.png"));
        botonInvisible(btnFoto);
        for (int i = 1800; i <= 2017; i++) {
            String s = Integer.toString(i);
            comboBoxAño.addItem(s);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoSexo = new javax.swing.ButtonGroup();
        radioBtnHombre = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        lblAsterisco1 = new javax.swing.JLabel();
        lblAsterisco3 = new javax.swing.JLabel();
        lblAsterisco2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtApellido = new javax.swing.JTextField();
        btnVolver = new javax.swing.JButton();
        txtLugarNacimiento = new javax.swing.JTextField();
        comboBoxDia = new javax.swing.JComboBox<>();
        comboBoxMes = new javax.swing.JComboBox<>();
        comboBoxAño = new javax.swing.JComboBox<>();
        comboBoxNacionalidad = new javax.swing.JComboBox<>();
        botonAceptar = new javax.swing.JButton();
        btnFoto = new javax.swing.JButton();
        lblMujer = new javax.swing.JLabel();
        lblHombre = new javax.swing.JLabel();
        lblFondo = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(1024, 768));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        grupoSexo.add(radioBtnHombre);
        radioBtnHombre.setFont(new java.awt.Font("Avenir", 0, 15)); // NOI18N
        radioBtnHombre.setSelected(true);
        radioBtnHombre.setContentAreaFilled(false);
        radioBtnHombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioBtnHombreActionPerformed(evt);
            }
        });
        add(radioBtnHombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 620, 30, -1));

        grupoSexo.add(jRadioButton2);
        jRadioButton2.setFont(new java.awt.Font("Avenir", 0, 15)); // NOI18N
        jRadioButton2.setContentAreaFilled(false);
        add(jRadioButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 620, -1, -1));

        lblAsterisco1.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        lblAsterisco1.setForeground(new java.awt.Color(204, 0, 0));
        lblAsterisco1.setText("*");
        add(lblAsterisco1, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 280, 20, 30));

        lblAsterisco3.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        lblAsterisco3.setForeground(new java.awt.Color(204, 0, 0));
        lblAsterisco3.setText("*");
        add(lblAsterisco3, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 620, 20, 30));

        lblAsterisco2.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        lblAsterisco2.setForeground(new java.awt.Color(204, 0, 0));
        lblAsterisco2.setText("*");
        add(lblAsterisco2, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 350, 20, 30));

        txtNombre.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });
        add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 270, 290, 40));

        txtApellido.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        txtApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoKeyTyped(evt);
            }
        });
        add(txtApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 340, 290, 40));

        btnVolver.setBorderPainted(false);
        btnVolver.setContentAreaFilled(false);
        btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });
        add(btnVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 660, 160, 40));

        txtLugarNacimiento.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        txtLugarNacimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtLugarNacimientoKeyTyped(evt);
            }
        });
        add(txtLugarNacimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 480, 290, 40));

        comboBoxDia.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        comboBoxDia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "28", "29", "30", "31", " " }));
        comboBoxDia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                comboBoxDiaKeyTyped(evt);
            }
        });
        add(comboBoxDia, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 410, 70, 30));

        comboBoxMes.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        comboBoxMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        comboBoxMes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxMesActionPerformed(evt);
            }
        });
        comboBoxMes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                comboBoxMesKeyTyped(evt);
            }
        });
        add(comboBoxMes, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 410, 70, 30));

        comboBoxAño.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        comboBoxAño.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-" }));
        comboBoxAño.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                comboBoxAñoKeyTyped(evt);
            }
        });
        add(comboBoxAño, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 410, 110, 30));

        comboBoxNacionalidad.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        comboBoxNacionalidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-", "Afganistán", "Akrotiri", "Albania", "Alemania", "Andorra", "Angola", "Anguila", "Antártida", "Antigua y Barbuda", "Antillas Neerlandesas", "Arabia Saudí", "Arctic Ocean", "Argelia", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Islands", "Atlantic Ocean", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bahráin", "Bangladesh", "Barbados", "Bélgica", "Belice", "Benín", "Bermudas", "Bielorrusia", "Birmania; Myanmar", "Bolivia", "Bosnia y Hercegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Chad", "Chile", "China", "Chipre", "Clipperton Island", "Colombia", "Comoras", "Congo", "Coral Sea Islands", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dhekelia", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "El Vaticano", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Gaza Strip", "Georgia", "Ghana", "Gibraltar", "Granada", "Grecia", "Groenlandia", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Ecuatorial", "Guinea-Bissau", "Guyana", "Haití", "Honduras", "Hong Kong", "Hungría", "India", "Indian Ocean", "Indonesia", "Irán", "Iraq", "Irlanda", "Isla Bouvet", "Isla Christmas", "Isla Norfolk", "Islandia", "Islas Caimán", "Islas Cocos", "Islas Cook", "Islas Feroe", "Islas Georgia del Sur y Sandwich del Sur", "Islas Heard y McDonald", "Islas Malvinas", "Islas Marianas del Norte", "Islas Marshall", "Islas Pitcairn", "Islas Salomón", "Islas Turcas y Caicos", "Islas Vírgenes Americanas", "Islas Vírgenes Británicas", "Israel", "Italia", "Jamaica", "Jan Mayen", "Japón", "Jersey", "Jordania", "Kazajistán", "Kenia", "Kirguizistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Macao", "Macedonia", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Man, Isle of", "Marruecos", "Mauricio", "Mauritania", "Mayotte", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Montserrat", "Mozambique", "Mundo", "Namibia", "Nauru", "Navassa Island", "Nepal", "Nicaragua", "Níger", "Nigeria", "Niue", "Noruega", "Nueva Caledonia", "Nueva Zelanda", "Omán", "Pacific Ocean", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa-Nueva Guinea", "Paracel Islands", "Paraguay", "Perú", "Polinesia Francesa", "Polonia", "Portugal", "Puerto Rico", "Qatar", "Reino Unido", "República Centroafricana", "República Checa", "República Democrática del Congo", "República Dominicana", "Ruanda", "Rumania", "Rusia", "Sáhara Occidental", "Samoa", "Samoa Americana", "San Cristóbal y Nieves", "San Marino", "San Pedro y Miquelón", "San Vicente y las Granadinas", "Santa Helena", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Southern Ocean", "Spratly Islands", "Sri Lanka", "Suazilandia", "Sudáfrica", "Sudán", "Suecia", "Suiza", "Surinam", "Svalbard y Jan Mayen", "Tailandia", "Taiwán", "Tanzania", "Tayikistán", "Territorio Británico del Océano Indico", "Territorios Australes Franceses", "Timor Oriental", "Togo", "Tokelau", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Unión Europea", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Wake Island", "Wallis y Futuna", "West Bank", "Yemen", "Yibuti", "Zambia", "Zimbabue" }));
        comboBoxNacionalidad.setMinimumSize(new java.awt.Dimension(120, 50));
        comboBoxNacionalidad.setPreferredSize(new java.awt.Dimension(140, 90));
        comboBoxNacionalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                comboBoxNacionalidadKeyTyped(evt);
            }
        });
        add(comboBoxNacionalidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 550, 290, 30));

        botonAceptar.setBorderPainted(false);
        botonAceptar.setContentAreaFilled(false);
        botonAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        botonAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAceptarActionPerformed(evt);
            }
        });
        add(botonAceptar, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 660, 140, 40));

        btnFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/subirFoto.png"))); // NOI18N
        btnFoto.setBorderPainted(false);
        btnFoto.setContentAreaFilled(false);
        btnFoto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFotoActionPerformed(evt);
            }
        });
        add(btnFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(506, 80, 133, 150));

        lblMujer.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        lblMujer.setText("Mujer");
        add(lblMujer, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 620, -1, -1));

        lblHombre.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        lblHombre.setText("Hombre");
        add(lblHombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 620, -1, -1));

        lblFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/registrarPersona.png"))); // NOI18N
        add(lblFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void botonAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAceptarActionPerformed
        registrarPersona();
    }//GEN-LAST:event_botonAceptarActionPerformed

    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        presionarEnter(evt);
    }//GEN-LAST:event_txtNombreKeyTyped

    private void txtApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoKeyTyped
        presionarEnter(evt);
    }//GEN-LAST:event_txtApellidoKeyTyped

    private void txtLugarNacimientoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLugarNacimientoKeyTyped
        presionarEnter(evt);
    }//GEN-LAST:event_txtLugarNacimientoKeyTyped

    private void comboBoxDiaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboBoxDiaKeyTyped
        presionarEnter(evt);
    }//GEN-LAST:event_comboBoxDiaKeyTyped

    private void comboBoxMesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboBoxMesKeyTyped
        presionarEnter(evt);
    }//GEN-LAST:event_comboBoxMesKeyTyped

    private void comboBoxAñoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboBoxAñoKeyTyped
        presionarEnter(evt);
    }//GEN-LAST:event_comboBoxAñoKeyTyped

    private void comboBoxNacionalidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboBoxNacionalidadKeyTyped
        presionarEnter(evt);
    }//GEN-LAST:event_comboBoxNacionalidadKeyTyped

    private void btnFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFotoActionPerformed
        JFileChooser objeto = new JFileChooser();
        objeto.setFileFilter(filter);
        int opcion = objeto.showOpenDialog(this);
        if (opcion == JFileChooser.APPROVE_OPTION) {
            File archivo = objeto.getSelectedFile();
            fotoPerfil = new ImageIcon(archivo.getPath());
            Image img = fotoPerfil.getImage();
            Image newimg = img.getScaledInstance(133, 150, java.awt.Image.SCALE_SMOOTH);
            fotoPerfil = new ImageIcon(newimg);
            btnFoto.setIcon(fotoPerfil);
            try {
                FileInputStream inStream = new FileInputStream(archivo);
                File archivoDestino = new File(System.getProperty("user.dir") + "/src/usuarios", archivo.getName());
                direccionFotoPerfil = System.getProperty("user.dir") + "/src/usuarios/" + archivo.getName();
                FileOutputStream outStream = new FileOutputStream(archivoDestino);
                byte[] buffer = new byte[1024];
                int largo;
                while ((largo = inStream.read(buffer)) > 0) {
                    outStream.write(buffer, 0, largo);
                }
            } catch (IOException ex) {
                //mostrarJOptionPane
            }
        }
    }//GEN-LAST:event_btnFotoActionPerformed

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        padre.removeAll();
        padre.add(new Arbol(padre, sistema, null));
        padre.updateUI();
    }//GEN-LAST:event_btnVolverActionPerformed

    private void comboBoxMesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxMesActionPerformed
        comboBoxDia.removeAllItems();
        comboBoxDia.addItem("-");
        if (comboBoxMes.getSelectedIndex() == 1 || comboBoxMes.getSelectedIndex() == 3 || comboBoxMes.getSelectedIndex() == 5 || comboBoxMes.getSelectedIndex() == 7 || comboBoxMes.getSelectedIndex() == 8 || comboBoxMes.getSelectedIndex() == 10 || comboBoxMes.getSelectedIndex() == 12) {
            for (int i = 1; i <= 31; i++) {
                String s = Integer.toString(i);
                comboBoxDia.addItem(s);
            }
        }
        if (comboBoxMes.getSelectedIndex() == 2) {
            
            for (int i = 1; i <= 29; i++) {
                String s = Integer.toString(i);
                comboBoxDia.addItem(s);
            }
        }
        if (comboBoxMes.getSelectedIndex() == 4 || comboBoxMes.getSelectedIndex() == 6 || comboBoxMes.getSelectedIndex() == 9 || comboBoxMes.getSelectedIndex() == 11) {
            for (int i = 1; i <= 30; i++) {
                String s = Integer.toString(i);
                comboBoxDia.addItem(s);
            }
        }
    }//GEN-LAST:event_comboBoxMesActionPerformed

    private void radioBtnHombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioBtnHombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioBtnHombreActionPerformed

    //Método para registrar una persona
    public void registrarPersona() {
        boolean valido = true;
        String mensajeError = "";
        Persona miPersona = new Persona();
        miPersona.setNombre(txtNombre.getText());
        miPersona.setApellido(txtApellido.getText());
        if (!(txtLugarNacimiento.getText().equals("")) && !(txtLugarNacimiento.getText().trim().length() == 0)) {
            miPersona.setLugarNacimiento(txtLugarNacimiento.getText());
        } else {
            miPersona.setLugarNacimiento("-");
        }
        if (comboBoxDia.getSelectedItem().equals("-") && comboBoxMes.getSelectedItem().equals("-") && comboBoxAño.getSelectedItem().equals("-")) {
            miPersona.setFechaNacimiento("-");
        } else {
            String fechaString = ((comboBoxDia.getSelectedItem()).toString() + "/" + (comboBoxMes.getSelectedItem()).toString() + "/" + (comboBoxAño.getSelectedItem()).toString());
            miPersona.setFechaNacimiento(fechaString);
        }
        miPersona.setNacionalidad(comboBoxNacionalidad.getSelectedItem().toString());
        miPersona.setDireccionFoto(direccionFotoPerfil);
        if (radioBtnHombre.isSelected()) {
            miPersona.setSexo(1);
        } else {
            miPersona.setSexo(2);
        }
        if ((comboBoxDia.getSelectedIndex() == 0 || comboBoxMes.getSelectedIndex() == 0 || comboBoxAño.getSelectedIndex() == 0) && !(comboBoxDia.getSelectedIndex() == 0 && comboBoxMes.getSelectedIndex() == 0 && comboBoxAño.getSelectedIndex() == 0)) {
            valido = false;
            mensajeError = "Debe ingresar una fecha de nacimiento válida.";
        }
        if (txtNombre.getText().trim().length() == 0 || txtApellido.getText().trim().length() == 0) {
            valido = false;
            mensajeError = "Debe completar los campos obligatorios";
        }
        if (!valido) {
            JOptionPane.showMessageDialog(padre, mensajeError, "Error", 0);
        } else {
            sistema.agregarPersona(miPersona);
            sistema.agregarPersonaSinPadres(miPersona);
            sistema.agregarPersonaSoltera(miPersona);
            JOptionPane.showMessageDialog(null, "La persona se ha registrado correctamente", "Registro jugador", 1);
            padre.removeAll();
            padre.add(new Arbol(padre, sistema, null));
            padre.updateUI();
        }
    }

    private void presionarEnter(java.awt.event.KeyEvent evt) {
        char teclaPresionada = evt.getKeyChar();
        if (teclaPresionada == KeyEvent.VK_ENTER) {
            botonAceptar.doClick();
        }
    }

    private void botonInvisible(JButton boton) {
        boton.setOpaque(false);
        boton.setContentAreaFilled(false);
        boton.setBorderPainted(false);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAceptar;
    private javax.swing.JButton btnFoto;
    private javax.swing.JButton btnVolver;
    private javax.swing.JComboBox<String> comboBoxAño;
    private javax.swing.JComboBox<String> comboBoxDia;
    private javax.swing.JComboBox<String> comboBoxMes;
    private javax.swing.JComboBox<String> comboBoxNacionalidad;
    private javax.swing.ButtonGroup grupoSexo;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JLabel lblAsterisco1;
    private javax.swing.JLabel lblAsterisco2;
    private javax.swing.JLabel lblAsterisco3;
    private javax.swing.JLabel lblFondo;
    private javax.swing.JLabel lblHombre;
    private javax.swing.JLabel lblMujer;
    private javax.swing.JRadioButton radioBtnHombre;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtLugarNacimiento;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
