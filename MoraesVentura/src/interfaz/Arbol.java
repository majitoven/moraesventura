package interfaz;

import dominio.*;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.*;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

/**
 *
 * @author mariaventura
 */
public class Arbol extends javax.swing.JPanel {

    private JPanel padre;
    private Sistema sistema;
    private Union volverAUnion;

    public Arbol(JPanel panelPadre, Sistema sis, Union u) {
        volverAUnion = u;
        sistema = sis;
        initComponents();
        padre = panelPadre;
        cargarPantallaInicio();
        personalizarIFrames();


        jScrollPaneTexto.setVisible(false);
        listaTexto.setBackground(new Color(195, 236, 236));
        listaTexto.setVisible(false);
        btnVerModoTexto.setVisible(false);
        btnOcultarModoTexto.setVisible(false);
        
                if (volverAUnion != null) {
            listaTexto.setVisible(true);
            btnVerModoTexto.setVisible(true);
            btnOcultarModoTexto.setVisible(false);
            cargarArbol(volverAUnion);

        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnRegistrar = new javax.swing.JButton();
        btnRelacion = new javax.swing.JButton();
        btnVerRecuerdos = new javax.swing.JButton();
        jScrollPaneTexto = new javax.swing.JScrollPane();
        listaTexto = new javax.swing.JList();
        btnAgregarRecuerdos = new javax.swing.JButton();
        btnOcultarModoTexto = new javax.swing.JButton();
        btnVerModoTexto = new javax.swing.JButton();
        btnArbolVolver = new javax.swing.JButton();
        jInternalFrameNuevo = new javax.swing.JInternalFrame();
        jInternalFrameArbol = new javax.swing.JInternalFrame();
        lblPregunta = new javax.swing.JLabel();
        comboBoxRaicesCandidatas = new javax.swing.JComboBox();
        labelFondo = new javax.swing.JLabel();

        setMinimumSize(new java.awt.Dimension(1024, 768));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnRegistrar.setBorderPainted(false);
        btnRegistrar.setContentAreaFilled(false);
        btnRegistrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });
        add(btnRegistrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 540, 160, 100));

        btnRelacion.setBorderPainted(false);
        btnRelacion.setContentAreaFilled(false);
        btnRelacion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRelacion.setName(""); // NOI18N
        btnRelacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelacionActionPerformed(evt);
            }
        });
        add(btnRelacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 650, 160, 100));

        btnVerRecuerdos.setBorderPainted(false);
        btnVerRecuerdos.setContentAreaFilled(false);
        btnVerRecuerdos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnVerRecuerdos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerRecuerdosActionPerformed(evt);
            }
        });
        add(btnVerRecuerdos, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 650, 110, 40));

        listaTexto.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        listaTexto.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPaneTexto.setViewportView(listaTexto);

        add(jScrollPaneTexto, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, 180, 280));

        btnAgregarRecuerdos.setBorderPainted(false);
        btnAgregarRecuerdos.setContentAreaFilled(false);
        btnAgregarRecuerdos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAgregarRecuerdos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarRecuerdosActionPerformed(evt);
            }
        });
        add(btnAgregarRecuerdos, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 700, 110, 40));

        btnOcultarModoTexto.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        btnOcultarModoTexto.setText("Ocultar modo texto");
        btnOcultarModoTexto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOcultarModoTextoActionPerformed(evt);
            }
        });
        add(btnOcultarModoTexto, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 190, 170, 40));

        btnVerModoTexto.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        btnVerModoTexto.setText("Ver modo texto");
        btnVerModoTexto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerModoTextoActionPerformed(evt);
            }
        });
        add(btnVerModoTexto, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 190, 170, 40));

        btnArbolVolver.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        btnArbolVolver.setText("Volver");
        btnArbolVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnArbolVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnArbolVolverActionPerformed(evt);
            }
        });
        add(btnArbolVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 570, 540, -1));

        jInternalFrameNuevo.setBorder(null);
        jInternalFrameNuevo.setVisible(true);
        add(jInternalFrameNuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 170, 560, 390));

        jInternalFrameArbol.setBorder(null);
        jInternalFrameArbol.setPreferredSize(new java.awt.Dimension(640, 480));
        jInternalFrameArbol.setVisible(true);
        jInternalFrameArbol.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblPregunta.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        lblPregunta.setForeground(new java.awt.Color(0, 102, 102));
        lblPregunta.setText("Seleccione la raíz del arbol que desea visualizar:");
        jInternalFrameArbol.getContentPane().add(lblPregunta, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 120, 390, -1));

        comboBoxRaicesCandidatas.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        comboBoxRaicesCandidatas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxRaicesCandidatasActionPerformed(evt);
            }
        });
        jInternalFrameArbol.getContentPane().add(comboBoxRaicesCandidatas, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 170, 340, -1));

        add(jInternalFrameArbol, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 170, 560, 390));
        add(labelFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnRelacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelacionActionPerformed
        if (sistema.largoListaPersonasSolteras() < 2 && sistema.largoListaPersonas() < 3) {
            JOptionPane.showMessageDialog(padre, "Lo sentimos, en este momentos no es posible agregar nuevas relaciones. Por favor registre nuevas personas en el sistema.", "Error", 0);
        } else {
            padre.removeAll();
            padre.add(new AgregarRelacion(padre, sistema));
            padre.updateUI();
        }
    }//GEN-LAST:event_btnRelacionActionPerformed

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
        padre.removeAll();
        padre.add(new RegistrarPersona(padre, sistema));
      padre.updateUI();    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void btnAgregarRecuerdosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarRecuerdosActionPerformed
        if (sistema.largoListaPersonas() == 0) {
            JOptionPane.showMessageDialog(padre, "Debe haber al menos una persona registrada en el sistema.", "Error", 0);
        } else {
            padre.removeAll();
            padre.add(new AgregarRecuerdo(padre, sistema, null));
            padre.updateUI();
        }
    }//GEN-LAST:event_btnAgregarRecuerdosActionPerformed

    private void btnVerRecuerdosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerRecuerdosActionPerformed
        if (!sistema.hayRecuerdos()) {
            JOptionPane.showMessageDialog(padre, "No hay recuerdos para visualizar.", "Error", 0);
        } else {
            padre.removeAll();
            padre.add(new VerRecuerdosDe(padre, sistema));
            padre.updateUI();
        }
    }//GEN-LAST:event_btnVerRecuerdosActionPerformed

    private void comboBoxRaicesCandidatasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxRaicesCandidatasActionPerformed
        if (comboBoxRaicesCandidatas.getSelectedIndex() != -1) {
            jInternalFrameArbol.setVisible(false);
            cargarArbol((Union) comboBoxRaicesCandidatas.getSelectedItem());
            // btnArbolVolver.setVisible(true);
        }
    }//GEN-LAST:event_comboBoxRaicesCandidatasActionPerformed

    private void btnArbolVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnArbolVolverActionPerformed
        comboBoxRaicesCandidatas.setSelectedIndex(-1);
        padre.removeAll();
        padre.add(new Arbol(padre, sistema, null));
        padre.updateUI();
    }//GEN-LAST:event_btnArbolVolverActionPerformed

    private void btnVerModoTextoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerModoTextoActionPerformed
        jScrollPaneTexto.setVisible(true);
        listaTexto.setVisible(true);
        btnVerModoTexto.setVisible(false);
        btnOcultarModoTexto.setVisible(true);
    }//GEN-LAST:event_btnVerModoTextoActionPerformed

    private void btnOcultarModoTextoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOcultarModoTextoActionPerformed
        jScrollPaneTexto.setVisible(false);
        listaTexto.setVisible(false);
        btnVerModoTexto.setVisible(true);
        btnOcultarModoTexto.setVisible(false);
    }//GEN-LAST:event_btnOcultarModoTextoActionPerformed

    private void cargarArbol(Union raiz) {
        btnArbolVolver.setVisible(true);
        if (raiz != null) {
            DefaultMutableTreeNode base = new DefaultMutableTreeNode(raiz);

            for (int i = 0; i < raiz.getListaHijos().size(); i++) {
                cargarNodos(raiz.getListaHijos().get(i), base);
            }
            JTree arbol = new JTree(base);

            btnVerModoTexto.setVisible(true);
            ArrayList<String> arbolTexto = new ArrayList<>();

            arbolTexto.add(0, raiz.getPersona1().toString());
            arbolTexto.add(1, raiz.getPersona2().toString());

            for (int i = 0; i < raiz.getListaHijos().size(); i++) {
                cargarModoTexto(raiz.getListaHijos().get(i), arbolTexto);
            }

            listaTexto.setListData(arbolTexto.toArray());

            //Selection listener
            selectionListener(arbol, raiz);
            //Mouse listener
            mouseListener(arbol);
            //Expansion listener
            expansionListener(arbol);

            //Propiedades
            arbol.setBackground(new Color(195, 236, 236));
            arbol.setRowHeight(50);
            arbol.setCellRenderer(new IconRenderer());
            JScrollPane scrollPane = new JScrollPane(arbol);
            scrollPane.setPreferredSize(new Dimension(640, 480));
            scrollPane.setBackground(new Color(195, 236, 236));
            jInternalFrameNuevo.setVisible(true);
            jInternalFrameNuevo.getContentPane().removeAll();
            jInternalFrameNuevo.getContentPane().add(scrollPane);
        }
    }

    private void selectionListener(JTree arbol, Union raiz) {
        arbol.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        arbol.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) arbol.getLastSelectedPathComponent();
                if (node == null) {
                    return;
                }
                Object nodoSeleccionado = node.getUserObject();
                if (nodoSeleccionado.getClass().equals(Persona.class)) {
                    padre.removeAll();
                    padre.add(new PerfilPersona(padre, sistema, (Persona) nodoSeleccionado, raiz));
                    padre.updateUI();
                } else {
                    Union u = (Union) nodoSeleccionado;
                    padre.removeAll();
                    padre.add(new PerfilPersona(padre, sistema, u.getPersona1(), raiz));
                    padre.updateUI();
                }
            }
        });
    }

    private void mouseListener(JTree arbol) {
        arbol.addMouseMotionListener(new MouseMotionListener() {

            public void mouseDragged(MouseEvent e) {

            }

            public void mouseMoved(MouseEvent e) {
                TreePath tp = ((JTree) e.getSource()).getPathForLocation(e.getX(), e.getY());

                if (tp != null) {
                    ((JTree) e.getSource()).setCursor(new Cursor(Cursor.HAND_CURSOR));
                } else {
                    ((JTree) e.getSource()).setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            }
        });
    }

    private void expansionListener(JTree arbol) {
        for (int i = 0; i < arbol.getRowCount(); i++) {
            arbol.expandRow(i);
        }
        arbol.setUI(new BasicTreeUI() {
            @Override
            protected boolean shouldPaintExpandControl(final TreePath path, final int row,
                    final boolean isExpanded, final boolean hasBeenExpanded, final boolean isLeaf) {
                boolean shouldDisplayExpandControl = false;
                return shouldDisplayExpandControl;
            }
        });
    }

    private void cargarNodos(Persona raiz, DefaultMutableTreeNode base) {
        if (raiz != null) {
            Union u = sistema.getPareja(raiz);
            if (u != null) {
                DefaultMutableTreeNode nodo = new DefaultMutableTreeNode(u);
                base.add(nodo);
                for (int i = 0; i < u.getListaHijos().size(); i++) {
                    cargarNodos(u.getListaHijos().get(i), nodo);
                }
            } else {
                DefaultMutableTreeNode hoja = new DefaultMutableTreeNode(raiz);
                base.add(hoja);
            }
        }
    }

    private void personalizarIFrames() {
        btnArbolVolver.setVisible(false);
        jInternalFrameNuevo.setBackground(new Color(1.0f, 1.0f, 1.0f, 0.5f));
        jInternalFrameNuevo.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        jInternalFrameNuevo.setResizable(false);
        // borrar la bara
        jInternalFrameNuevo.setBorder(null);
        jInternalFrameNuevo.setVisible(false);
        jInternalFrameArbol.setBackground(new Color(1.0f, 1.0f, 1.0f, 0.5f));
        jInternalFrameArbol.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        jInternalFrameArbol.setResizable(false);
        // borrar la bara
        jInternalFrameArbol.setBorder(null);
        jInternalFrameArbol.setVisible(true);
    }

    private void cargarPantallaInicio() {
        if (sistema.getRaicesCandidatas().isEmpty()) {
            lblPregunta.setVisible(false);
            comboBoxRaicesCandidatas.setVisible(false);
            labelFondo.setIcon(new ImageIcon(getClass().getResource("/recursos/arbolVacio.png")));
        } else {
            labelFondo.setIcon(new ImageIcon(getClass().getResource("/recursos/arbol.png")));
            lblPregunta.setVisible(true);
            comboBoxRaicesCandidatas.setVisible(true);
            for (int i = 0; i < sistema.getRaicesCandidatas().size(); i++) {
                comboBoxRaicesCandidatas.addItem(sistema.getRaicesCandidatas().get(i));
            }
        }
        comboBoxRaicesCandidatas.setSelectedIndex(-1);
    }

    private void cargarModoTexto(Persona raiz, ArrayList<String> base) {
        if (raiz != null) {
            Union u = sistema.getPareja(raiz);
            if (u != null) {
                String p1 = u.getPersona1().toString();
                String p2 = u.getPersona2().toString();
                base.add(p1);
                base.add(p2);
                for (int i = 0; i < u.getListaHijos().size(); i++) {
                    cargarModoTexto(u.getListaHijos().get(i), base);
                }
            } else {
                String hoja = raiz.toString();
                base.add(hoja);
            }
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarRecuerdos;
    private javax.swing.JButton btnArbolVolver;
    private javax.swing.JButton btnOcultarModoTexto;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JButton btnRelacion;
    private javax.swing.JButton btnVerModoTexto;
    private javax.swing.JButton btnVerRecuerdos;
    private javax.swing.JComboBox comboBoxRaicesCandidatas;
    private javax.swing.JInternalFrame jInternalFrameArbol;
    private javax.swing.JInternalFrame jInternalFrameNuevo;
    private javax.swing.JScrollPane jScrollPaneTexto;
    private javax.swing.JLabel labelFondo;
    private javax.swing.JLabel lblPregunta;
    private javax.swing.JList listaTexto;
    // End of variables declaration//GEN-END:variables
}
