package interfaz;

import dominio.Historia;
import dominio.Persona;
import dominio.Sistema;
import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author mariaventura
 */
public class AgregarHistoria extends javax.swing.JPanel {

    private Sistema sistema;
    private JPanel padre;

    public AgregarHistoria(JPanel panelPadre, Sistema sis) {
        sistema = sis;
        padre = panelPadre;
        initComponents();
        jScrollPaneHistoria.setOpaque(false);
        jScrollPaneHistoria.getViewport().setOpaque(false);
        txtContenido.setOpaque(false);
        txtContenido.setLineWrap(true);
        txtContenido.setWrapStyleWord(true);
        txtTitulo.setHorizontalAlignment(JTextField.CENTER);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnVolver = new javax.swing.JButton();
        lblAsterisco1 = new javax.swing.JLabel();
        lblTitulo = new javax.swing.JLabel();
        btnAceptar = new javax.swing.JButton();
        txtTitulo = new javax.swing.JTextField();
        jScrollPaneHistoria = new javax.swing.JScrollPane();
        txtContenido = new javax.swing.JTextArea();
        lblFondo = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnVolver.setBorderPainted(false);
        btnVolver.setContentAreaFilled(false);
        btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });
        add(btnVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 700, 160, 50));

        lblAsterisco1.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        lblAsterisco1.setForeground(new java.awt.Color(204, 0, 0));
        lblAsterisco1.setText("*");
        add(lblAsterisco1, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 140, 20, 30));

        lblTitulo.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        lblTitulo.setText("Titulo:");
        add(lblTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 142, -1, -1));

        btnAceptar.setBorderPainted(false);
        btnAceptar.setContentAreaFilled(false);
        btnAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        add(btnAceptar, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 700, 140, 50));

        txtTitulo.setBackground(new java.awt.Color(204, 197, 136));
        txtTitulo.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        txtTitulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTituloActionPerformed(evt);
            }
        });
        add(txtTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 140, 310, -1));

        jScrollPaneHistoria.setBorder(null);
        jScrollPaneHistoria.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPaneHistoria.setMaximumSize(new java.awt.Dimension(23, 8));

        txtContenido.setColumns(20);
        txtContenido.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        txtContenido.setRows(5);
        txtContenido.setBorder(null);
        txtContenido.setMaximumSize(new java.awt.Dimension(0, 22));
        jScrollPaneHistoria.setViewportView(txtContenido);

        add(jScrollPaneHistoria, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 180, 370, 470));

        lblFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/agregarHistoria.png"))); // NOI18N
        add(lblFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void txtTituloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTituloActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTituloActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        agregarHistoria();
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        padre.removeAll();
        padre.add(new AgregarRecuerdo(padre, sistema, null));
        padre.updateUI();
    }//GEN-LAST:event_btnVolverActionPerformed

    private void agregarHistoria() {
        boolean valido = true;
        String mensajeError = "";
        Historia miHistoria = new Historia();
        miHistoria.setTitulo(txtTitulo.getText());
        miHistoria.setContenido(txtContenido.getText());
        if (txtTitulo.getText().trim().length() == 0 || txtContenido.getText().trim().length() == 0) {
            valido = false;
            mensajeError = "Debe ingresar un titulo y un contenido.";
        }
        if (!valido) {
            JOptionPane.showMessageDialog(padre, mensajeError, "Error", 0);
        } else {
            JOptionPane.showMessageDialog(null, "La historia se ha registrado correctamente", "Agregar historia", 1);
            padre.removeAll();
            padre.add(new AgregarRecuerdo(padre, sistema, miHistoria));
            padre.updateUI();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnVolver;
    private javax.swing.JScrollPane jScrollPaneHistoria;
    private javax.swing.JLabel lblAsterisco1;
    private javax.swing.JLabel lblFondo;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JTextArea txtContenido;
    private javax.swing.JTextField txtTitulo;
    // End of variables declaration//GEN-END:variables
}
