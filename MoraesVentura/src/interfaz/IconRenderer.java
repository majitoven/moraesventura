package interfaz;

import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import dominio.Persona;
import dominio.Union;
import java.net.URL;
import static java.rmi.server.RMIClassLoader.getClassLoader;

/**
 *
 * @author mariaventura
 */
public class IconRenderer implements TreeCellRenderer {

    private final JLabel label;

    public IconRenderer() {
        label = new JLabel();
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hashFocus) {
        int clase = 0;//clase 1.Persona, clase 2.Union
        Object o = ((DefaultMutableTreeNode) value).getUserObject();
        Persona p = null;
        Union u = null;
        if (o.getClass().equals(Persona.class)) {
            p = (Persona) o;
            clase = 1;
        } else {
            u = (Union) o;
            clase = 2;
        }
        URL imageUrl=null;
        if (clase == 1) {
            if (p.getSexo() == 1) {
                imageUrl = getClass().getClassLoader().getResource("recursos/Hombre.png");
            } else {
                imageUrl = getClass().getClassLoader().getResource("recursos/Mujer.png");
            }
            label.setText(p.toString());
        } else {
            if (u.getTipo() == 0) {
                imageUrl = getClass().getClassLoader().getResource("recursos/Casados.png");
            } else if (u.getTipo() == 1) {
                imageUrl = getClass().getClassLoader().getResource("recursos/Concubinos.png");
            }
            label.setText(u.toString());
        }
        if (imageUrl != null) {
            ImageIcon i = new ImageIcon(imageUrl);
            //Image img = i.getImage();
            //Image newimg = img.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
            //i = new ImageIcon(newimg);
            label.setIcon(i);
            label.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        }
        return label;
    }
}